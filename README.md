# NetRadius Java Commons

This library provides useful and reusable Java components with minimal dependencies.
This library is not a replacement for [Apache Commons](https://commons.apache.org/). The components in 
this project were either not found, not as simple as desired or not as performant
as required.

## Downloads
Releases of this library are published  on [Maven Central](https://search.maven.org/).

 * Release Repository - https://repo.maven.apache.org/maven2/com/netradius/java-commons/
 * Staging Repository - https://oss.sonatype.org/content/groups/staging/com/netradius/java-commons/
 * Snapshot Repository - https://oss.sonatype.org/content/repositories/snapshots/com/netradius/java-commons/
 
## Java Compatibility

This project tests on Java LTS releases only. At present this library targets the Java 8 runtime and
is tested to work on Java 11.

## Bugs & Enhancement Requests

Please file any bugs or enhancements at https://bitbucket.org/netradius/java-commons/issues

## License
This project is licensed under the BSD 3-Clause License. See [LICENSE.txt](https://bitbucket.org/netradius/java-commons/src/master/LICENSE.txt)
