/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.bitsnbytes;

import com.netradius.commons.io.IOHelper;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper methods for manipulating, searching and displaying bytes.
 *
 * @author Erik R. Jensen
 */
public class BitTwiddler {

  protected static final int DEFAULT_BUFFER_SIZE = 1024;
  protected static final int SHORT_BYTES = 2;
  protected static final int INT_BYTES = 4;
  protected static final int LONG_BYTES = 8;

  public static final byte[] BASE64_CHARS = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
      'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
      'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
      'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
      '='
  };

  public static final byte[] URL_MODIFIED_BASE64_CHARS = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
      'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
      'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
      'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_',
  };

  public static final byte[] YUI_BASE64_CHARS = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
      'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
      'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
      'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '_',
      '-'
  };

  protected static final byte[] upperHexChars = {
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
  };

  protected static final byte[] lowerHexChars = {
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
  };

  protected static final byte[] invertedHexChars = new byte['f' + 1];

  static {
    for (int i = upperHexChars.length - 1; i >= 0; i--) {
      invertedHexChars[upperHexChars[i]] = (byte) i;
    }
    for (int i = lowerHexChars.length - 1; i >= 0; i--) {
      invertedHexChars[lowerHexChars[i]] = (byte) i;
    }
  }

  protected static final byte[] binChars = {'0', '1'};

  /**
   * Reverses a series of bytes. This method modifies the array passed into it. The
   * return value is just a convenience.
   *
   * @param buf the array to be reversed
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the byte array reversed
   */
  public static byte[] reverse(byte[] buf, int fromIndex, int toIndex) {
    for (int i = toIndex - 1; i >= fromIndex + (toIndex - fromIndex) / 2; i--) {
      int o = toIndex - 1 - i + fromIndex;
      byte b = buf[i];
      buf[i] = buf[o];
      buf[o] = b;
    }
    return buf;
  }

  /**
   * Reverses an array of bytes.
   *
   * @param buf the byte array
   * @return the byte array
   */
  public static byte[] reverse(byte[] buf) {
    return reverse(buf, 0, buf.length);
  }

  private static long leto(byte[] buf, int fromIndex, int length) {
    long val = 0;
    for (int i = 0; i < length; i++) {
      val += (long) (buf[fromIndex + i] & 0xff) << i * 8;
    }
    return val;
  }

  private static long leto(ByteBuffer buf, int fromIndex, int length) {
    long val = 0;
    buf.position(fromIndex);
    for (int i = 0; i < length; i++) {
      val += (long) (buf.get() & 0xff) << i * 8;
    }
    return val;
  }

  /**
   * Converts bytes from little-endian to a long.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long letol(byte[] buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > LONG_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a long.");
    }
    return leto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from little-endian to a long.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long letol(ByteBuffer buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > LONG_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a long.");
    }
    return leto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from little-endian to a long.
   *
   * @param buf the byte array
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long letol(byte[] buf) {
    return letol(buf, 0, buf.length);
  }

  /**
   * Converts bytes from little-endian to a long.
   *
   * @param buf the byte array
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long letol(ByteBuffer buf) {
    return letol(buf, 0, buf.remaining());
  }

  /**
   * Converts bytes from little-endian to an integer.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int letoi(byte[] buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > INT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into an int.");
    }
    return (int) leto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from little-endian to an integer.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int letoi(ByteBuffer buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > INT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into an int.");
    }
    return (int) leto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from little-endian to an integer.
   *
   * @param buf the byte array
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int letoi(byte[] buf) {
    return letoi(buf, 0, buf.length);
  }

  /**
   * Converts bytes from little-endian to an integer.
   *
   * @param buf the byte array
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int letoi(ByteBuffer buf) {
    return letoi(buf, 0, buf.remaining());
  }

  /**
   * Converts bytes from little-endian to a short.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short letos(byte[] buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > SHORT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a short.");
    }
    return (short) leto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from little-endian to a short.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short letos(ByteBuffer buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > SHORT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a short.");
    }
    return (short) leto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from little-endian to a short.
   *
   * @param buf the byte array
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short letos(byte[] buf) {
    return letos(buf, 0, buf.length);
  }

  /**
   * Converts bytes from little-endian to a short.
   *
   * @param buf the byte array
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short letos(ByteBuffer buf) {
    return letos(buf, 0, buf.remaining());
  }

  private static long beto(byte[] buf, int fromIndex, int length) {
    long val = 0;
    for (int i = 0; i < length; i++) {
      val += (long) (buf[fromIndex + i] & 0xff) << ((length - 1) - i) * 8;
    }
    return val;
  }

  private static long beto(ByteBuffer buf, int fromIndex, int length) {
    long val = 0;
    buf.position(fromIndex);
    for (int i = 0; i < length; i++) {
      val += (long) (buf.get() & 0xff) << ((length - 1) - i) * 8;
    }
    return val;
  }

  /**
   * Converts bytes from big-endian to a long.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long betol(byte[] buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > LONG_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a long.");
    }
    return beto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from big-endian to a long.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long betol(ByteBuffer buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > LONG_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a long.");
    }
    return beto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from big-endian to a long.
   *
   * @param buf the byte array
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long betol(byte[] buf) {
    return betol(buf, 0, buf.length);
  }

  /**
   * Converts bytes from big-endian to a long.
   *
   * @param buf the byte array
   * @return the long value
   * @throws IllegalArgumentException if the number of bytes exceeds 8
   */
  public static long betol(ByteBuffer buf) {
    return betol(buf, 0, buf.remaining());
  }

  /**
   * Converts bytes from big-endian to an integer.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int betoi(byte[] buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > INT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into an int.");
    }
    return (int) beto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from big-endian to an integer.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int betoi(ByteBuffer buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > INT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into an int.");
    }
    return (int) beto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from big-endian to an integer.
   *
   * @param buf the byte array
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int betoi(byte[] buf) {
    return betoi(buf, 0, buf.length);
  }

  /**
   * Converts bytes from big-endian to an integer.
   *
   * @param buf the byte array
   * @return the integer value
   * @throws IllegalArgumentException if the number of bytes exceeds 4
   */
  public static int betoi(ByteBuffer buf) {
    return betoi(buf, 0, buf.remaining());
  }

  /**
   * Converts bytes from big-endian to a short.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short betos(byte[] buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > SHORT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a short.");
    }
    return (short) beto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from big-endian to a short.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short betos(ByteBuffer buf, int fromIndex, int toIndex) {
    final int length = toIndex - fromIndex;
    if (length > SHORT_BYTES) {
      throw new IllegalArgumentException(length + " bytes will not fit into a short.");
    }
    return (short) beto(buf, fromIndex, length);
  }

  /**
   * Converts bytes from big-endian to a short.
   *
   * @param buf the byte array
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short betos(byte[] buf) {
    return betos(buf, 0, buf.length);
  }

  /**
   * Converts bytes from big-endian to a short.
   *
   * @param buf the byte array
   * @return the short value
   * @throws IllegalArgumentException if the number of bytes exceeds 2
   */
  public static short betos(ByteBuffer buf) {
    return betos(buf, 0, buf.remaining());
  }

  /**
   * Converts a long to a little-endian array of bytes.
   *
   * @param val the value to convert
   * @return the bytes of the value
   */
  public static byte[] ltole(long val) {
    final byte[] buf = new byte[8];
    for (int i = 7; i >= 0; i--) {
      buf[i] = (byte) (val >> (i * 8));
    }
    return buf;
  }

  /**
   * Converts an integer to a little-endian array of bytes.
   *
   * @param val the value to convert
   * @return the bytes of the value
   */
  public static byte[] itole(int val) {
    final byte[] buf = new byte[4];
    for (int i = 3; i >= 0; i--) {
      buf[i] = (byte) (val >> (i * 8));
    }
    return buf;
  }

  /**
   * Converts a short to a little-endian array of bytes.
   *
   * @param val the value to convert
   * @return the bytes of the value
   */
  public static byte[] stole(short val) {
    final byte[] buf = new byte[2];
    buf[1] = (byte) (val >> 8);
    buf[0] = (byte) val;
    return buf;
  }

  /**
   * Converts a long to a big-endian array of bytes.
   *
   * @param val the value to convert
   * @return the bytes of the value
   */
  public static byte[] ltobe(long val) {
    final byte[] buf = new byte[8];
    for (int i = 7; i >= 0; i--) {
      buf[i] = (byte) (val >> (7 - i) * 8);
    }
    return buf;
  }

  /**
   * Converts an integer to a big-endian array of bytes.
   *
   * @param val the value to convert
   * @return the bytes of the value
   */
  public static byte[] itobe(int val) {
    final byte[] buf = new byte[4];
    for (int i = 3; i >= 0; i--) {
      buf[i] = (byte) (val >> (3 - i) * 8);
    }
    return buf;
  }

  /**
   * Converts a short to a big-endian array of bytes.
   *
   * @param val the value to convert
   * @return the bytes of the value
   */
  public static byte[] stobe(int val) {
    final byte[] buf = new byte[2];
    buf[0] = (byte) (val >> 8);
    buf[1] = (byte) val;
    return buf;
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @param fromIndex the start index (inclusive) to encode
   * @param toIndex the end index (exclusive) to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static byte[] tob64(final byte[] buf, final int fromIndex, final int toIndex,
      byte[] b64chars) {
    if (b64chars.length != 65 && b64chars.length != 64) {
      throw new IllegalArgumentException("Base 64 character sets must be 64 or 65 characters.");
    }
    // every 3 bytes is 4 characters in padded base64 (6 bits per char)
    final int num = toIndex - fromIndex;
    final int numc = b64chars.length == 65
        ? (num + 2 - ((num + 2) % 3)) / 3 * 4 // padded
        : num * 8 / 6 + (num * 8 % 6 != 0 ? 1 : 0); // not padded
    final byte[] b64 = new byte[numc];
    int n = 0;
    for (int i = 0; i < toIndex; i += 3) {
      int v = (buf[i] & 0xff) << 16 | (i + 1 < toIndex ? (buf[i + 1] & 0xff) << 8 : 0) | (
          i + 2 < toIndex ? (buf[i + 2] & 0xff) : 0);
      b64[n++] = b64chars[v >>> 18 & 0x3f];
      b64[n++] = b64chars[v >>> 12 & 0x3f];
      switch (toIndex - i) { // calculate bytes remaining to be processed
        case 1: // 0 bytes left to process
          if (b64chars.length == 65) {
            b64[n++] = b64chars[64];
            b64[n++] = b64chars[64];
          }
          break;
        case 2: // 1 byte left to process
          b64[n++] = b64chars[v >>> 6 & 0x3f];
          if (b64chars.length == 65) {
            b64[n++] = b64chars[64];
          }
          break;
        default:
          b64[n++] = b64chars[v >>> 6 & 0x3f];
          b64[n++] = b64chars[v & 0x3f];
          break;
      }
    }
    return b64;
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @param fromIndex the start index (inclusive) to encode
   * @param toIndex the end index (exclusive) to encode
   * @return the base64 string
   */
  public static byte[] tob64(final byte[] buf, final int fromIndex, final int toIndex) {
    return tob64(buf, fromIndex, toIndex, BASE64_CHARS);
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static byte[] tob64(byte[] buf, byte[] b64chars) {
    return tob64(buf, 0, buf.length, b64chars);
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @return the base64 string
   */
  public static byte[] tob64(byte[] buf) {
    return tob64(buf, 0, buf.length);
  }

  /**
   * Base64 encodes a long value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static byte[] tob64(long val, byte[] b64chars) {
    return tob64(ltobe(val), b64chars);
  }

  /**
   * Base64 encodes a long value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static byte[] tob64(long val) {
    return tob64(ltobe(val));
  }

  /**
   * Base64 encodes an integer value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static byte[] tob64(int val, byte[] b64chars) {
    return tob64(itobe(val), b64chars);
  }

  /**
   * Base64 encodes an integer value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static byte[] tob64(int val) {
    return tob64(itobe(val));
  }

  /**
   * Base64 encodes a short value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static byte[] tob64(short val, byte[] b64chars) {
    return tob64(stobe(val), b64chars);
  }

  /**
   * Base64 encodes a short value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static byte[] tob64(short val) {
    return tob64(stobe(val));
  }

  /**
   * Base64 encodes a byte value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static byte[] tob64(byte val, byte[] b64chars) {
    return tob64(new byte[]{val}, b64chars);
  }

  /**
   * Base64 encodes a byte value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static byte[] tob64(byte val) {
    return tob64(new byte[]{val});
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @param fromIndex the start index (inclusive) to encode
   * @param toIndex the end index (exclusive) to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static String tob64str(byte[] buf, int fromIndex, int toIndex, byte[] b64chars) {
    return new String(tob64(buf, fromIndex, toIndex, b64chars));
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @param fromIndex the start index (inclusive) to encode
   * @param toIndex the end index (exclusive) to encode
   * @return the base64 string
   */
  public static String tob64str(byte[] buf, int fromIndex, int toIndex) {
    return new String(tob64(buf, fromIndex, toIndex));
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static String tob64str(byte[] buf, byte[] b64chars) {
    return new String(tob64(buf, b64chars));
  }

  /**
   * Base64 encodes a series of bytes.
   *
   * @param buf the bytes to encode
   * @return the base64 string
   */
  public static String tob64str(byte[] buf) {
    return new String(tob64(buf));
  }

  /**
   * Base64 encodes a byte value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static String tob64str(byte val, byte[] b64chars) {
    return new String(tob64(val, b64chars));
  }

  /**
   * Base64 encodes a byte value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static String tob64str(byte val) {
    return new String(tob64(val));
  }

  /**
   * Base64 encodes a short value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static String tob64str(short val, byte[] b64chars) {
    return new String(tob64(val, b64chars));
  }

  /**
   * Base64 encodes a short value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static String tob64str(short val) {
    return new String(tob64(val));
  }

  /**
   * Base64 encodes an int value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static String tob64str(int val, byte[] b64chars) {
    return new String(tob64(val, b64chars));
  }

  /**
   * Base64 encodes an int value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static String tob64str(int val) {
    return new String(tob64(val));
  }

  /**
   * Base64 encodes a long value.
   *
   * @param val the value to encode
   * @param b64chars the base64 character set to use for encoding
   * @return the base64 string
   */
  public static String tob64str(long val, byte[] b64chars) {
    return new String(tob64(val, b64chars));
  }

  /**
   * Base64 encodes a long value.
   *
   * @param val the value to encode
   * @return the base64 string
   */
  public static String tob64str(long val) {
    return new String(tob64(val));
  }

  /**
   * Encodes a series of bytes to hexadecimal characters.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be converted
   * @param toIndex the index of the last element (exclusive) to be converted
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return the hexadecimal characters
   */
  public static byte[] tohex(byte[] buf, int fromIndex, int toIndex, boolean lower) {
    final byte[] chars = lower ? lowerHexChars : upperHexChars;
    final byte[] hex = new byte[(toIndex - fromIndex) * 2];
    final int last = toIndex - 1;
    for (int i = last; i >= fromIndex; i--) {
      int idx = buf[i] & 0xff;
      int j = (i - fromIndex) * 2;
      hex[j] = chars[idx >> 4];
      hex[j + 1] = chars[idx & 0x0f];
    }
    return hex;
  }

  /**
   * Encodes an array of bytes to hexadecimal characters. This method will return hexadecimal
   * with uppercase letters.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be encoded
   * @param toIndex the index of the last element (exclusive) to be encoded
   * @return the hexadecimal characters
   */
  public static byte[] tohex(byte[] buf, int fromIndex, int toIndex) {
    return tohex(buf, fromIndex, toIndex, false);
  }

  /**
   * Converts an array of bytes to hexadecimal characters.
   *
   * @param buf the byte array
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return the hexadecimal characters
   */
  public static byte[] tohex(byte[] buf, boolean lower) {
    return tohex(buf, 0, buf.length, lower);
  }

  /**
   * Converts an array of bytes to hexadecimal characters. This method will return hexadecimal
   * using uppercase letters.
   *
   * @param buf the byte array
   * @return the hexadecimal characters
   */
  public static byte[] tohex(byte[] buf) {
    return tohex(buf, 0, buf.length, false);
  }

  /**
   * Converts a long to hexadecimal. This method will return hexadecimal using uppercase letters.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return the hexadecimal characters
   */
  public static byte[] tohex(long val, boolean lower) {
    return tohex(ltobe(val), lower);
  }

  /**
   * Converts a long to hexadecimal. This method will return hexadecimal using uppercase letters.
   *
   * @param val the value to convert
   * @return the hexadecimal characters
   */
  public static byte[] tohex(long val) {
    return tohex(val, false);
  }

  /**
   * Converts an int to hexadecimal.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return the hexadecimal characters
   */
  public static byte[] tohex(int val, boolean lower) {
    return tohex(itobe(val), lower);
  }

  /**
   * Converts an int to hexadecimal. This method will return hexadecimal using uppercase letters.
   *
   * @param val the value to convert
   * @return the hexadecimal characters
   */
  public static byte[] tohex(int val) {
    return tohex(val, false);
  }

  /**
   * Converts a short to hexadecimal.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return the hexadecimal characters
   */
  public static byte[] tohex(short val, boolean lower) {
    return tohex(stobe(val), lower);
  }

  /**
   * Converts a short to hexadecimal. This method will return hexadecimal using uppercase letters.
   *
   * @param val the value to convert
   * @return the hexadecimal characters
   */
  public static byte[] tohex(short val) {
    return tohex(val, false);
  }

  /**
   * Converts a byte to hexadecimal.
   *
   * @param val the value to convert
   * @return the hexadecimal characters
   */
  public static byte[] tohex(byte val) {
    return tohex(val, false);
  }

  /**
   * Converts a byte to hexadecimal.
   *
   * @param val the value to convert
   * @param lower true to use uppercase letters, false to use lowercase letters
   * @return the hexadecimal characters
   */
  public static byte[] tohex(byte val, boolean lower) {
    return tohex(new byte[]{val}, lower);
  }

  /**
   * Converts a series of bytes to a hexadecimal string.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a hexadecimal string
   */
  public static String tohexstr(byte[] buf, int fromIndex, int toIndex, boolean lower,
      boolean pad) {
    final byte[] chars = tohex(buf, fromIndex, toIndex, lower);
    if (!pad) {
      for (int i = 0; i < chars.length; i++) {
        if (chars[i] != '0' || i == chars.length - 1) {
          return new String(chars, i, chars.length - i);
        }
      }
    }
    return new String(chars);
  }

  /**
   * Converts an array of bytes to a hexadecimal string.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return a hexadecimal string
   */
  public static String tohexstr(byte[] buf, int fromIndex, int toIndex, boolean lower) {
    return tohexstr(buf, fromIndex, toIndex, lower, true);
  }

  /**
   * Converts an array of bytes to a hexadecimal string.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return a hexadecimal string
   */
  public static String tohexstr(byte[] buf, int fromIndex, int toIndex) {
    return tohexstr(buf, fromIndex, toIndex, false, true);
  }

  /**
   * Converts an array of bytes to a hexadecimal string.
   *
   * @param buf the byte array
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a hexadecimal string
   */
  public static String tohexstr(byte[] buf, boolean lower, boolean pad) {
    return tohexstr(buf, 0, buf.length, lower, pad);
  }

  /**
   * Converts an array of bytes to a hexadecimal string.
   *
   * @param buf the byte array
   * @param lower the offset into the byte array
   * @return a hexadecimal string
   */
  public static String tohexstr(byte[] buf, boolean lower) {
    return tohexstr(buf, 0, buf.length, lower, true);
  }

  /**
   * Converts an array of bytes to a hexadecimal string.
   *
   * @param buf the byte array
   * @return a hexadecimal string
   */
  public static String tohexstr(byte[] buf) {
    return tohexstr(buf, 0, buf.length, false, true);
  }

  /**
   * Converts a byte to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a hexadecimal string
   */
  public static String tohexstr(byte val, boolean lower, boolean pad) {
    return tohexstr(new byte[]{val}, lower, pad);
  }

  /**
   * Converts a byte to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return a hexadecimal string
   */
  public static String tohexstr(byte val, boolean lower) {
    return tohexstr(val, lower, true);
  }

  /**
   * Converts a byte to a hexadecimal string.
   *
   * @param val the value to convert
   * @return a hexadecimal string
   */
  public static String tohexstr(byte val) {
    return tohexstr(val, false, true);
  }

  /**
   * Converts a short to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a hexadecimal string
   */
  public static String tohexstr(short val, boolean lower, boolean pad) {
    return tohexstr(stobe(val), lower, pad);
  }

  /**
   * Converts a short to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return a hexadecimal string
   */
  public static String tohexstr(short val, boolean lower) {
    return tohexstr(val, lower, true);
  }

  /**
   * Converts a short to a hexadecimal string.
   *
   * @param val the value to convert
   * @return a hexadecimal string
   */
  public static String tohexstr(short val) {
    return tohexstr(val, false, true);
  }

  /**
   * Converts an int to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a hexadecimal string
   */
  public static String tohexstr(int val, boolean lower, boolean pad) {
    return tohexstr(itobe(val), lower, pad);
  }

  /**
   * Converts an int to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return a hexadecimal string
   */
  public static String tohexstr(int val, boolean lower) {
    return tohexstr(val, lower, true);
  }

  /**
   * Converts an int to hexadecimal.
   *
   * @param val the value to convert
   * @return a hexadecimal string
   */
  public static String tohexstr(int val) {
    return tohexstr(val, false, true);
  }

  /**
   * Converts a long to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a hexadecimal string
   */
  public static String tohexstr(long val, boolean lower, boolean pad) {
    return tohexstr(ltobe(val), lower, pad);
  }

  /**
   * Converts a long to a hexadecimal string.
   *
   * @param val the value to convert
   * @param lower true to use lowercase letters, false to use uppercase letters
   * @return a hexadcimal string
   */
  public static String tohexstr(long val, boolean lower) {
    return tohexstr(val, lower, true);
  }

  /**
   * Converts a long to a hexadecimal string.
   *
   * @param val the value to convert
   * @return a hexadecimal string
   */
  public static String tohexstr(long val) {
    return tohexstr(val, false, true);
  }

  /**
   * Decodes a series of bytes from hexadecimal.
   *
   * @param buf the bytes to decode
   * @param fromIndex the index of the first element (inclusive) to be converted
   * @param toIndex the index of the last element (exclusive) to be converted
   * @return the decoded bytes
   */
  public static byte[] fromhex(byte[] buf, int fromIndex, int toIndex) {
    int len = toIndex - fromIndex;
    if (len < 0) {
      throw new IllegalArgumentException("A negative length was specified.");
    }
    if (len % 2 != 0) {
      throw new IllegalArgumentException("Odd number of characters found.");
    }
    final byte[] unhex = new byte[(toIndex - fromIndex) / 2];
    for (int i = toIndex - 1; i >= fromIndex; i -= 2) {
      byte b1 = invertedHexChars[buf[i - 1]];
      byte b2 = invertedHexChars[buf[i]];
      unhex[i / 2] = (byte) ((b1 << 4) + b2);
    }
    return unhex;
  }

  /**
   * Decodes an array of bytes from hexadecimal.
   *
   * @param buf the byte array
   * @return the decoded bytes
   */
  public static byte[] fromhex(byte[] buf) {
    return fromhex(buf, 0, buf.length);
  }

  /**
   * Decodes a hexadecimal string.
   *
   * @param hex the hexadecimal string to decode
   * @return the decoded bytes
   */
  public static byte[] fromhex(String hex) {
    return fromhex(hex.getBytes(StandardCharsets.US_ASCII));
  }

  /**
   * Decodes an array of bytes from hexadecimal.
   *
   * @param buf the byte array
   * @return the decoded long value
   */
  public static long lfromhex(byte[] buf) {
    return betol(fromhex(buf));
  }

  /**
   * Decodes a hexadecimal string.
   *
   * @param hex the hexadecimal string to decode
   * @return the decoded long value
   */
  public static long lfromhex(String hex) {
    return lfromhex(hex.getBytes(StandardCharsets.US_ASCII));
  }

  /**
   * Decodes an array of bytes from hexadecimal.
   *
   * @param buf the byte array
   * @return the decoded integer
   */
  public static int ifromhex(byte[] buf) {
    return betoi(fromhex(buf));
  }

  /**
   * Decodes a hexadecimal string.
   *
   * @param hex the hexadecimal string to decode
   * @return the decoded integer
   */
  public static int ifromhex(String hex) {
    return ifromhex(hex.getBytes(StandardCharsets.US_ASCII));
  }

  /**
   * Decodes an array of bytes from hexadecimal.
   *
   * @param buf the byte array
   * @return lower true to use lowercase letters, false to use uppercase letters
   */
  public static short sfromhex(byte[] buf) {
    return betos(fromhex(buf));
  }

  /**
   * Decodes a hexadecimal string.
   *
   * @param hex the hexadecimal string to decode
   * @return the decoded short value
   */
  public static short sfromhex(String hex) {
    return sfromhex(hex.getBytes(StandardCharsets.US_ASCII));
  }

  /**
   * Decodes an array og bytes from hexadecimal.
   *
   * @param buf the byte array to decode
   * @return the decoded byte
   */
  public static byte bfromhex(byte[] buf) {
    return fromhex(buf, 0, buf.length)[0];
  }

  /**
   * Decodes a hexadecimal string.
   *
   * @param hex the hexadecimal to decode
   * @return the decoded byte value
   */
  public static byte bfromhex(String hex) {
    return bfromhex(hex.getBytes(StandardCharsets.US_ASCII));
  }

  /**
   * Converts an array of bytes to binary.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the binary characters
   */
  public static byte[] tobin(byte[] buf, int fromIndex, int toIndex) {
    final byte[] bin = new byte[(toIndex - fromIndex) * 8];
    for (int i = toIndex - 1; i >= fromIndex; i--) {
      int idx = buf[i] & 0xff;
      int j = (i - fromIndex) * 8;
      bin[j] = binChars[(idx >> 7) & 0x1];
      bin[j + 1] = binChars[(idx >> 6) & 0x1];
      bin[j + 2] = binChars[(idx >> 5) & 0x1];
      bin[j + 3] = binChars[(idx >> 4) & 0x1];
      bin[j + 4] = binChars[(idx >> 3) & 0x1];
      bin[j + 5] = binChars[(idx >> 2) & 0x1];
      bin[j + 6] = binChars[(idx >> 1) & 0x1];
      bin[j + 7] = binChars[idx & 0x1];
    }
    return bin;
  }

  /**
   * Converts an array of bytes to binary.
   *
   * @param buf the byte array
   * @return the binary characters
   */
  public static byte[] tobin(byte[] buf) {
    return tobin(buf, 0, buf.length);
  }

  /**
   * Converts a long to binary.
   *
   * @param val the value to convert
   * @return the binary characters
   */
  public static byte[] tobin(long val) {
    return tobin(ltobe(val));
  }

  /**
   * Converts an integer to binary.
   *
   * @param val the value to convert
   * @return the binary characters
   */
  public static byte[] tobin(int val) {
    return tobin(itobe(val));
  }

  /**
   * Converts a short to binary.
   *
   * @param val the value to convert
   * @return the binary characters
   */
  public static byte[] tobin(short val) {
    return tobin(stobe(val));
  }

  /**
   * Converts a byte to binary.
   *
   * @param val the value to convert
   * @return the binary characters
   */
  public static byte[] tobin(byte val) {
    return tobin(new byte[]{val});
  }

  /**
   * Converts an array of bytes to a binary string.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a binary string
   */
  public static String tobinstr(byte[] buf, int fromIndex, int toIndex, boolean pad) {
    final byte[] chars = tobin(buf, fromIndex, toIndex);
    if (!pad) {
      for (int i = 0; i < chars.length; i++) {
        if (chars[i] != '0' || chars.length - i == 1) {
          return new String(chars, i, chars.length - i);
        }
      }
    }
    return new String(chars);
  }

  /**
   * Converts an array of bytes to a binary string.
   *
   * @param buf the byte array
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return a binary string
   */
  public static String tobinstr(byte[] buf, int fromIndex, int toIndex) {
    return tobinstr(buf, fromIndex, toIndex, false);
  }

  /**
   * Converts an array of bytes to a binary string.
   *
   * @param buf the byte array
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a binary string
   */
  public static String tobinstr(byte[] buf, boolean pad) {
    return tobinstr(buf, 0, buf.length, pad);
  }

  /**
   * Converts an array of bytes to a binary string.
   *
   * @param buf the byte array
   * @return a binary string
   */
  public static String tobinstr(byte[] buf) {
    return tobinstr(buf, 0, buf.length);
  }

  /**
   * Converts a byte to binary.
   *
   * @param val the value to convert
   * @param pad true to show all bytes, false to remove leading 0s
   * @return the binary characters
   */
  public static String tobinstr(byte val, boolean pad) {
    return tobinstr(new byte[]{val}, pad);
  }

  /**
   * Converts a byte to a binary string.
   *
   * @param val the value to convert
   * @return a binary string
   */
  public static String tobinstr(byte val) {
    return tobinstr(val, false);
  }

  /**
   * Converts a short to a binary string.
   *
   * @param val the value to convert
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a binary string
   */
  public static String tobinstr(short val, boolean pad) {
    return tobinstr(stobe(val), pad);
  }

  /**
   * Converts a short to a binary string.
   *
   * @param val the value to convert
   * @return a binary string
   */
  public static String tobinstr(short val) {
    return tobinstr(val, false);
  }

  /**
   * Converts an int to a binary string.
   *
   * @param val the value to convert
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a binary string
   */
  public static String tobinstr(int val, boolean pad) {
    return tobinstr(itobe(val), pad);
  }

  /**
   * Converts an int to a binary string.
   *
   * @param val the value to convert
   * @return a binary string
   */
  public static String tobinstr(int val) {
    return tobinstr(val, false);
  }

  /**
   * Converts a long to a binary string.
   *
   * @param val the value to convert
   * @param pad true to show all bytes, false to remove leading 0s
   * @return a binary string
   */
  public static String tobinstr(long val, boolean pad) {
    return tobinstr(ltobe(val), pad);
  }

  /**
   * Converts a long to a binary string.
   *
   * @param val the value to convert
   * @return a binary string
   */
  public static String tobinstr(long val) {
    return tobinstr(val, false);
  }

  /**
   * Returns the first offset where the byte pattern is found in a subset of an array of bytes.
   *
   * @param b the bytes to search
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @param p the pattern to search for
   * @return the offset where the pattern is found, or a -1 if not found
   */
  public static int indexOf(final byte[] b, int fromIndex, int toIndex, final byte[] p) {
    final List<Integer> offsets = search(b, fromIndex, toIndex, p, 1);
    return offsets.size() == 1 ? offsets.get(0) : -1;
  }

  /**
   * Returns the first offset where the byte pattern is found in the bytes.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @return the offset where the pattern is found, or -1 if not found
   */
  public static int indexOf(final byte[] b, final byte[] p) {
    return indexOf(b, 0, b.length, p);
  }

  /**
   * Returns the first max offsets where the byte pattern is found in a subset of an array of
   * bytes in the order they were found.
   *
   * @param b the bytes to search
   * @param fromIndex the index to start searching (inclusive)
   * @param toIndex the index to search to (exclusive)
   * @param p the pattern to search for
   * @param max the maximum number of matches or a negative number for no maximum
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final int fromIndex, final int toIndex,
      final byte[] p, final int max) {
    if (p.length > (toIndex - fromIndex)) {
      new ArrayList<Integer>(0);
    }
    final List<Integer> offsets =
        max == -1 ? new ArrayList<Integer>() : new ArrayList<Integer>(max);
    for (int i = fromIndex + p.length; i < toIndex; i++) {
      for (int j = p.length - 1; j >= 0; j--) {
        int idx = i - (p.length - j);
        if (p[j] != b[idx]) {
          break;
        } else if (j == 0) {
          offsets.add(idx);
          if (offsets.size() == max) {
            return offsets;
          }
        }
      }
    }
    return offsets;
  }

  /**
   * Returns all offsets where the byte pattern is found in a subset of an array of bytes in the
   * order they were found.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final int fromIndex, final int toIndex,
      final byte[] p) {
    return search(b, fromIndex, toIndex, p, -1);
  }

  /**
   * Returns all offsets where the byte pattern is found in the array of bytes in the order they
   * were found.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final byte[] p) {
    return search(b, 0, b.length, p, -1);
  }

  /**
   * Returns the first max offsets where the byte pattern is found in an array of bytes in the
   * order they were found.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @param max the maximum number of matches or a negative number for no maximum
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final byte[] p, final int max) {
    return search(b, 0, b.length, p, max);
  }

  /**
   * Returns all offsets where the byte pattern is found in the stream of bytes in the order they
   * were found. This method will search to the end of the stream. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream.
   *
   * @param in the input stream to search
   * @param p the pattern to search for
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final byte[] p) throws IOException {
    return search(in, -1, p, new byte[DEFAULT_BUFFER_SIZE]);
  }

  /**
   * Returns all offsets where the pattern is found in the stream of bytes in the order they were
   * found. This method will search until len bytes have been read. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream.
   *
   * @param in the input stream to search
   * @param len the number of bytes to read
   * @param p the pattern to search for
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final long len, final byte[] p)
      throws IOException {
    return search(in, len, p, new byte[DEFAULT_BUFFER_SIZE]);
  }

  /**
   * Returns all offsets where the pattern is found in the stream of bytes in the order they were
   * found. This method will search until the end of the stream. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream.
   *
   * @param in the input stream to search
   * @param p the pattern to search for
   * @param b a buffer to use during searching
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final byte[] p, final byte[] b)
      throws IOException {
    return search(in, -1, p, b);
  }

  /**
   * Returns all offsets where the pattern is found in the stream of bytes in the order they were
   * found. This method will search until len bytes have been read. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream.
   *
   * @param in the input stream to search
   * @param len the number of bytes to read
   * @param p the pattern to search for
   * @param b the buffer to use during searching
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, long len, final byte[] p, final byte[] b)
      throws IOException {
    if (len == -1) {
      len = Long.MAX_VALUE;
    }
    if (b.length <= p.length) {
      throw new IllegalArgumentException("Buffer size must be larger than the pattern size.");
    }
    if (len < p.length) {
      throw new IllegalArgumentException("Length to read must be larger than the pattern size.");
    }
    final List<Long> offsets = new ArrayList<Long>();
    long l = len;
    int r;
    int tr;
    int off = 0;
    do {
      tr = l < b.length - off ? (int) l : b.length - off; // calc bytes to read
      r = IOHelper.fill(in, b, off, tr); // read bytes
      List<Integer> ioffsets = search(b, 0, off + r + 1, p);
      for (int i : ioffsets) {
        offsets.add(len - l - off + i);
      }
      l -= r; // reduce the length to read
      if (off + r >= p.length) {
        System.arraycopy(b, off + r - p.length, b, 0, p.length - 1);
        off = p.length;
      }
    } while (r == tr && l > 0); // end when EOF is reached or there is no more to read
    return offsets;
  }

  /**
   * Performs an xor on a number of byte arrays. All arrays passed to this method must be the
   * same length.
   *
   * @param arrays the byte arrays to xor
   * @return the result
   */
  public static byte[] xor(byte[]... arrays) {
    byte[] x = null;
    for (byte[] b : arrays) {
      if (x == null) {
        x = b;
      } else {
        for (int i = b.length - 1; i >= 0; i--) {
          x[i] ^= b[i];
        }
      }
    }
    return x;
  }

  /**
   * Finds index of a byte in the byte array.
   *
   * @param searchFrom the byte array to search from
   * @param searchFor byte to be searched
   * @return zero based index of byte to be searched, in the byte array
   */
  public static int searchbyte(byte[] searchFrom, byte searchFor) {
    if (searchFrom.length <= 0) {
      return -1;
    }
    for (int i = 0; i < searchFrom.length; i++) {
      if (searchFrom[i] == searchFor) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Decodes a series of Base64 encoded bytes.
   *
   * @param buf the bytes to decode
   * @param fromIndex the start index (inclusive) to decode
   * @param toIndex the end index (exclusive) to decode
   * @return the decoded characters byte array
   */
  public static byte[] fromb64(final byte[] buf, final int fromIndex, final int toIndex) {
    return fromb64(buf, fromIndex, toIndex, BASE64_CHARS);
  }

  /**
   * Decodes a series of Base64 encoded bytes.
   *
   * @param buf the bytes to decode
   * @param b64chars the base64 character set to use for decoding
   * @return the decoded characters byte array
   */
  public static byte[] fromb64(byte[] buf, byte[] b64chars) {
    int toIndex = 0;
    if (b64chars.length == 65) {
      toIndex = buf.length / 4 * 3 - (buf[buf.length - 2] == b64chars[64]
          ? 2 : buf[buf.length - 1] == b64chars[64] ? 1 : 0);
    } else {
      toIndex = buf.length / 4 * 3 + ((buf.length % 4) == 0 ? 0 : buf.length % 4 - 1);
    }
    return fromb64(buf, 0, toIndex, b64chars);
  }

  /**
   * Decodes a series of Base64 encoded bytes.
   *
   * @param buf the bytes to decode
   * @return the decoded characters byte array
   */
  public static byte[] fromb64(byte[] buf) {
    return fromb64(buf, BASE64_CHARS);
  }

  /**
   * Decodes a series of Base64 encoded bytes.
   *
   * @param buf the bytes to decode
   * @param fromIndex the start index (inclusive) to decode
   * @param toIndex the end index (exclusive) to decode
   * @param b64chars the base64 character set to use for decoding
   * @return the decoded characters byte array
   */
  public static byte[] fromb64(final byte[] buf, final int fromIndex, final int toIndex,
      byte[] b64chars) {
    if (b64chars.length != 64 && b64chars.length != 65) {
      throw new IllegalArgumentException("Base 64 character sets must be 64 or 65 characters.");
    }
    if (buf.length % 4 != 0 && b64chars.length == 65) {
      throw new IllegalArgumentException("Invalid base 64 string.");
    }

    final int num = toIndex - fromIndex;
    final int numc = b64chars.length == 65
        ? (num + 2 - ((num + 2) % 3)) / 3 * 4
        : num * 8 / 6 + (num * 8 % 6 != 0 ? 1 : 0);

    int n = 0;
    byte[] fromb64 = new byte[num];
    for (int i = 0; i < numc; i += 4) {
      int bytesToProcess = searchbyte(b64chars, buf[i]) << 18;
      if (i + 1 < buf.length) {
        bytesToProcess |= (searchbyte(b64chars, buf[i + 1]) << 12);
      }
      if (i + 2 < buf.length) {
        bytesToProcess |= (searchbyte(b64chars, buf[i + 2]) << 6);
      }
      if (i + 3 < buf.length) {
        bytesToProcess |= searchbyte(b64chars, buf[i + 3]);
      }
      fromb64[n++] = (byte) (bytesToProcess >>> 16 & 0xff);
      if (n < num) {
        fromb64[n++] = (byte) (bytesToProcess >>> 8 & 0xff);
      }
      if (n < num) {
        fromb64[n++] = (byte) (bytesToProcess & 0xff);
      }
    }
    return fromb64;
  }

  /**
   * Decodes a series of Base64 encoded bytes for a long.
   *
   * @param buf the bytes to decode
   * @param b64chars the base64 character set to use for decoding
   * @return long value of decoded bytes
   */
  public static long lfromb64(byte[] buf, byte[] b64chars) {
    byte[] fromb64 = fromb64(buf, b64chars);
    return betol(fromb64);
  }

  /**
   * Decodes a series of Base64 encoded bytes for a long.
   *
   * @param buf the bytes to decode
   * @return long value of decoded bytes
   */
  public static long lfromb64(byte[] buf) {
    return lfromb64(buf, BASE64_CHARS);
  }

  /**
   * Decodes a series of Base64 encoded bytes for a int.
   *
   * @param buf the bytes to decode
   * @param b64chars the base64 character set to use for decoding
   * @return int value of decoded bytes
   */
  public static int ifromb64(byte[] buf, byte[] b64chars) {
    byte[] fromb64 = fromb64(buf, b64chars);
    return betoi(fromb64, 0, fromb64.length);
  }

  /**
   * Decodes a series of Base64 encoded bytes for a int.
   *
   * @param buf the bytes to decode
   * @return int value of decoded bytes
   */
  public static int ifromb64(byte[] buf) {
    return ifromb64(buf, BASE64_CHARS);
  }

  /**
   * Decodes a series of Base64 encoded bytes for a short.
   *
   * @param buf the bytes to decode
   * @param b64chars the base64 character set to use for decoding
   * @return short value of decoded bytes
   */
  public static short sfromb64(byte[] buf, byte[] b64chars) {
    byte[] fromb64 = fromb64(buf, b64chars);
    return betos(fromb64, 0, fromb64.length);
  }

  /**
   * Decodes a series of Base64 encoded bytes for a short.
   *
   * @param buf the bytes to decode
   * @return short value of decoded bytes
   */
  public static short sfromb64(byte[] buf) {
    return sfromb64(buf, BASE64_CHARS);
  }

  /**
   * Decodes a series of Base64 encoded bytes for a byte.
   *
   * @param buf the bytes to decode
   * @param b64chars the base64 character set to use for decoding
   * @return byte value of decoded bytes
   */
  public static byte bfromb64(byte[] buf, byte[] b64chars) {
    byte[] fromb64 = fromb64(buf, b64chars);
    return fromb64[0];
  }

  /**
   * Decodes a series of Base64 encoded bytes for a byte.
   *
   * @param buf the bytes to decode
   * @return byte value of decoded bytes
   */
  public static byte bfromb64(byte[] buf) {
    return bfromb64(buf, BASE64_CHARS);
  }

  /**
   * Decodes a Base64 encoded string.
   *
   * @param b64str the Base64 encoded string
   * @param fromIndex the start index (inclusive) to decode
   * @param toIndex the end index (exclusive) to decode
   * @param b64chars Base64 character set to use for decoding
   * @return byte array of decoded bytes
   */
  public static byte[] fromb64str(String b64str, int fromIndex, int toIndex, byte[] b64chars) {
    return fromb64(b64str.getBytes(), fromIndex, toIndex, b64chars);
  }

  /**
   * Decodes a Base64 encoded string.
   *
   * @param b64str the Base64 encoded string
   * @param fromIndex the start index (inclusive) to decode
   * @param toIndex the end index (exclusive) to decode
   * @return byte array of decoded bytes
   */
  public static byte[] fromb64str(String b64str, int fromIndex, int toIndex) {
    return fromb64(b64str.getBytes(), fromIndex, toIndex, BASE64_CHARS);
  }

  /**
   * Decodes a Base64 encoded string.
   *
   * @param b64str the Base64 encoded string
   * @param b64chars Base64 character set to use for decoding
   * @return byte array of decoded bytes
   */
  public static byte[] fromb64str(String b64str, byte[] b64chars) {
    return fromb64(b64str.getBytes(), b64chars);
  }

  /**
   * Decodes a Base64 encoded string.
   *
   * @param b64str the Base64 encoded string
   * @return byte array of decoded bytes
   */
  public static byte[] fromb64str(String b64str) {
    return fromb64str(b64str, BASE64_CHARS);
  }

  /**
   * Decodes a Base64 encoded string for a byte.
   *
   * @param b64str the Base64 encoded string
   * @param b64chars Base64 character set to use for decoding
   * @return byte value of decoded bytes
   */
  public static byte bfromb64str(String b64str, byte[] b64chars) {
    return bfromb64(b64str.getBytes(), b64chars);
  }

  /**
   * Decodes a Base64 encoded string for a byte.
   *
   * @param b64str the Base64 encoded string
   * @return byte value of decoded bytes
   */
  public static byte bfromb64str(String b64str) {
    return bfromb64str(b64str, BASE64_CHARS);
  }

  /**
   * Decodes a Base64 encoded string for a short.
   *
   * @param b64str the Base64 encoded string
   * @param b64chars Base64 character set to use for decoding
   * @return short value of decoded bytes
   */
  public static short sfromb64str(String b64str, byte[] b64chars) {
    return sfromb64(b64str.getBytes(), b64chars);
  }

  /**
   * Decodes a Base64 encoded string for a short.
   *
   * @param b64str the Base64 encoded string
   * @return short value of decoded bytes
   */
  public static short sfromb64str(String b64str) {
    return sfromb64str(b64str, BASE64_CHARS);
  }

  /**
   * Decodes a Base64 encoded string for a int.
   *
   * @param b64str the Base64 encoded string
   * @param b64chars Base64 character set to use for decoding
   * @return int value of decoded bytes
   */
  public static int ifromb64str(String b64str, byte[] b64chars) {
    return ifromb64(b64str.getBytes(), b64chars);
  }

  /**
   * Decodes a Base64 encoded string for a int.
   *
   * @param b64str the Base64 encoded string
   * @return int value of decoded bytes
   */
  public static int ifromb64str(String b64str) {
    return ifromb64str(b64str, BASE64_CHARS);
  }

  /**
   * Decodes a Base64 encoded string for a long.
   *
   * @param b64str the Base64 encoded string
   * @param b64chars Base64 character set to use for decoding
   * @return long value of decoded bytes
   */
  public static long lfromb64str(String b64str, byte[] b64chars) {
    return lfromb64(b64str.getBytes(), b64chars);
  }

  /**
   * Decodes a Base64 encoded string for a long.
   *
   * @param b64str the Base64 encoded string
   * @return long value of decoded bytes
   */
  public static long lfromb64str(String b64str) {
    return lfromb64str(b64str, BASE64_CHARS);
  }
}
