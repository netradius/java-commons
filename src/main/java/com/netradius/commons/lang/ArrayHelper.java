/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import com.netradius.commons.bitsnbytes.BitTwiddler;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Random;

/**
 * This class contains common helper methods for dealing with arrays. It is meant to supplement
 * or provide better implementations to methods in{@link java.util.Arrays}.
 *
 * @author Erik R. Jensen
 */
public class ArrayHelper {

  private static final Random RAND = new Random();

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static byte[] concat(final byte[]... a) {
    int length = 0;
    for (byte[] b : a) {
      length += b.length;
    }
    final byte[] c = new byte[length];
    int idx = 0;
    for (byte[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static short[] concat(final short[]... a) {
    int length = 0;
    for (short[] b : a) {
      length += b.length;
    }
    final short[] c = new short[length];
    int idx = 0;
    for (short[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static int[] concat(final int[]... a) {
    int length = 0;
    for (int[] b : a) {
      length += b.length;
    }
    final int[] c = new int[length];
    int idx = 0;
    for (int[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static long[] concat(final long[]... a) {
    int length = 0;
    for (long[] b : a) {
      length += b.length;
    }
    final long[] c = new long[length];
    int idx = 0;
    for (long[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static float[] concat(final float[]... a) {
    int length = 0;
    for (float[] b : a) {
      length += b.length;
    }
    final float[] c = new float[length];
    int idx = 0;
    for (float[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static double[] concat(final double[]... a) {
    int length = 0;
    for (double[] b : a) {
      length += b.length;
    }
    final double[] c = new double[length];
    int idx = 0;
    for (double[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  public static char[] concat(final char[]... a) {
    int length = 0;
    for (char[] b : a) {
      length += b.length;
    }
    final char[] c = new char[length];
    int idx = 0;
    for (char[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Concatenates two arrays and returns the result. This method does not change the
   * existing arrays.
   *
   * @param clazz the type of the array
   * @param a the arrays to concatinate
   * @return the new concatinated array
   */
  @SafeVarargs
  public static <T> T[] concat(final Class<T> clazz, final T[]... a) {
    int length = 0;
    for (T[] b : a) {
      length += b.length;
    }
    @SuppressWarnings("unchecked") final T[] c = (T[]) Array.newInstance(clazz, length);
    int idx = 0;
    for (T[] b : a) {
      System.arraycopy(b, 0, c, idx, b.length);
      idx += b.length;
    }
    return c;
  }

  /**
   * Appends items to the end of an array. This method does not change the existing array.
   *
   * @param clazz the type of the array
   * @param a the array to append to
   * @param t the items to append
   * @return the new array with the appended values
   */
  @SafeVarargs
  public static <T> T[] append(final Class<T> clazz, final T[] a, T... t) {
    return concat(clazz, a, t);
  }

  /**
   * Reverses an array of bytes. The return value and argument are the same reference
   * for convenience.
   *
   * @param a the array to reverse
   * @return the reversed array
   */
  public static byte[] reverse(final byte... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      byte t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Reverses an array of shorts. The return value and argument are the same reference
   * for convenience.
   *
   * @param a the array to reverse
   * @return the reversed array
   */
  public static short[] reverse(final short... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      short t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Reverses an array of ints. The return value and argument are the same reference
   * for convenience.
   *
   * @param a the array to reverse
   * @return the reversed array
   */
  public static int[] reverse(final int... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      int t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Reverses an array of longs. The return value and argument are the same reference
   * for convenience.
   *
   * @param a the array to reverse
   * @return the reversed array
   */
  public static long[] reverse(final long... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      long t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Reverses an array of floats. The return values and argument are the same reference
   * for convenience.
   *
   * @param a the array to reverse
   * @return the reversed array
   */
  public static float[] reverse(final float... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      float t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Reverses an array of doubles. The return values and argument are the same reference
   * for convenience.
   *
   * @param a the array to reverse
   * @return the reversed array
   */
  public static double[] reverse(final double... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      double t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  public static char[] reverse(final char... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      char t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Reverses an array of objects. The return value and argument are the same references
   * for convenience.
   *
   * @param a the array to reverse
   * @param <T> the type contained in the array
   * @return the reversed array
   */
  @SafeVarargs
  public static <T> T[] reverse(final T... a) {
    for (int l = 0, r = a.length - 1; l < r; l++, r--) {
      T t = a[l];
      a[l] = a[r];
      a[r] = t;
    }
    return a;
  }

  /**
   * Returns the first offset where the byte pattern is found in a subset of an array of bytes.
   * This method calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#indexOf(byte[], int, int, byte[])}
   * and is provided on this class for convenience.
   *
   * @param b the bytes to search
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @param p the pattern to search for
   * @return the offset where the pattern is found, or a -1 if not found
   */
  public static int indexOf(final byte[] b, int fromIndex, int toIndex, final byte[] p) {
    return BitTwiddler.indexOf(b, fromIndex, toIndex, p);
  }

  /**
   * Returns the first offset where the byte pattern is found in the bytes. This method calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#indexOf(byte[], byte[])} and is provided
   * on this class for convenience.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @return the offset where the pattern is found, or -1 if not found
   */
  public static int indexOf(final byte[] b, final byte[] p) {
    return indexOf(b, 0, b.length, p);
  }

  /**
   * Returns the first max offsets where the byte pattern is found in a subset of an array of bytes
   * in the order they were found. This method calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(byte[], int, int, byte[], int)}
   * and is provided on this class for convenience.
   *
   * @param b the bytes to search
   * @param fromIndex the index to start searching (inclusive)
   * @param toIndex the index to search to (exclusive)
   * @param p the pattern to search for
   * @param max the maximum number of matches or a negative number for no maximum
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final int fromIndex, final int toIndex,
      final byte[] p, final int max) {
    return BitTwiddler.search(b, fromIndex, toIndex, p, max);
  }

  /**
   * Returns all offsets where the byte pattern is found in a subset of an array of bytes in the
   * order they were found. This method calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(byte[], int, int, byte[])}
   * and is provided on this class for convenience.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @param fromIndex the index of the first element (inclusive) to be searched
   * @param toIndex the index of the last element (exclusive) to be searched
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final int fromIndex, final int toIndex,
      final byte[] p) {
    return BitTwiddler.search(b, fromIndex, toIndex, p);
  }

  /**
   * Returns all offsets where the byte pattern is found in the array of bytes in the order they
   * were found. This method calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(byte[], byte[])}
   * and is provided on this class for convenience.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final byte[] p) {
    return BitTwiddler.search(b, p);
  }

  /**
   * Returns the first max offsets where the byte pattern is found in an array of bytes in the
   * order they were found. This method calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(byte[], byte[], int)}
   * and is provided on this class for convenience.
   *
   * @param b the bytes to search
   * @param p the pattern to search for
   * @param max the maximum number of matches or a negative number for no maximum
   * @return the offsets where the pattern is found
   */
  public static List<Integer> search(final byte[] b, final byte[] p, final int max) {
    return BitTwiddler.search(b, p, max);
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(byte[] a1, byte[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(short[] a1, short[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(int[] a1, int[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(long[] a1, long[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(float[] a1, float[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(double[] a1, double[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(char[] a1, char[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (a1[i] != a2[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Checks to see if the contents of two arrays are equal.
   *
   * @param a1 the first array
   * @param a2 the second array
   * @return true if the contents of the arrays are equal, false if otherwise
   */
  public static boolean equals(Object[] a1, Object[] a2) {
    if (a1.length == a2.length) {
      for (int i = a1.length - 1; i >= 0; i--) {
        if (!a1[i].equals(a2[i])) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  public static int[] shuffle(int... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      int tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  public static short[] shuffle(short... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      short tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  public static long[] shuffle(long... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      long tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  public static float[] shuffle(float... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      float tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  public static double[] shuffle(double... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      double tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  public static char[] shuffle(char... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      char tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }

  /**
   * Shuffles the given array randomizing the elements. The argument and return
   * values are the same for reference.
   *
   * @param a the array to shuffle
   * @return the shuffled array
   */
  @SafeVarargs
  public static <T> T[] shuffle(final T... a) {
    for (int i = 0; i < a.length; i++) {
      int p = RAND.nextInt(a.length);
      T tmp = a[i];
      a[i] = a[p];
      a[p] = tmp;
    }
    return a;
  }
}
