/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

import static com.netradius.commons.lang.StringHelper.*;

/**
 * This is a helper class which provides common method argument validations.
 *
 * @author Erik R. Jensen
 */
public class ValidationHelper {

  public static final String NOT_NULL_MSG = "%s may not be null";
  public static final String NOT_EMPTY_MSG = "%s may not be empty";
  public static final String NOT_BLANK_MSG = "%s may not be blank";
  public static final String MIN_MSG_D = "%s must be greater than or equal to %d";
  public static final String MIN_MSG_F = "%s must be greater than or equal to %f";
  public static final String MAX_MSG_D = "%s must be less than or equal to %d";
  public static final String MAX_MSG_F = "%s must be less than or equal to %f";
  public static final String RANGE_MSG_D = "%s must be less than %d and greater than %d";
  public static final String RANGE_MSG_F = "%s must be less than %f and greater than %f";
  public static final String REGEX = "%s must match %s";

  /**
   * Validates the given argument is not null.
   *
   * @param o the argument to validate
   * @param name the name of the argument
   * @param <T> the type of the argument
   */
  public static <T> void notNull(final T o, final String name) {
    notBlank(name, "name");
    if (o == null) {
      throw new IllegalArgumentException(String.format(NOT_NULL_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param s the argument to validate
   * @param name the argument name
   */
  public static void notEmpty(final CharSequence s, final String name) {
    notBlank(name, "name");
    if (isEmpty(s)) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument  is not blank.
   *
   * @param s the argument to validate
   * @param name the argument name
   */
  public static void notBlank(final CharSequence s, final String name) {
    if (isBlank(name)) {
      throw new IllegalArgumentException(String.format(NOT_BLANK_MSG, "name"));
    }
    if (isBlank(s)) {
      throw new IllegalArgumentException(String.format(NOT_BLANK_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   * @param <T> the type of the argument
   */
  public static <T> void notEmpty(final T[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final byte[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final int[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final short[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final long[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final float[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param a the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final double[] a, final String name) {
    notBlank(name, "name");
    if (a == null || a.length == 0) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param c the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final Collection<?> c, final String name) {
    notBlank(name, "name");
    if (c == null || c.isEmpty()) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is not empty.
   *
   * @param m the argument to validate
   * @param name the name of the argument
   */
  public static void notEmpty(final Map<?, ?> m, final String name) {
    notBlank(name, "name");
    if (m == null || m.isEmpty()) {
      throw new IllegalArgumentException(String.format(NOT_EMPTY_MSG, name));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final int num, final int min, final String name) {
    notBlank(name, "name");
    if (num < min) {
      throw new IllegalArgumentException(String.format(MIN_MSG_D, name, min));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final short num, final short min, final String name) {
    notBlank(name, "name");
    if (num < min) {
      throw new IllegalArgumentException(String.format(MIN_MSG_D, name, min));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final long num, final long min, final String name) {
    notBlank(name, "name");
    if (num < min) {
      throw new IllegalArgumentException(String.format(MIN_MSG_D, name, min));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final float num, final float min, final String name) {
    notBlank(name, "name");
    if (num < min) {
      throw new IllegalArgumentException(String.format(MIN_MSG_F, name, min));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final double num, final double min, final String name) {
    notBlank(name, "name");
    if (num < min) {
      throw new IllegalArgumentException(String.format(MIN_MSG_F, name, min));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final BigInteger num, final BigInteger min, final String name) {
    notBlank(name, "name");
    notNull(num, "num");
    notNull(min, "min");
    if (num.compareTo(min) < 0) {
      throw new IllegalArgumentException(String.format(MIN_MSG_D, name, min));
    }
  }

  /**
   * Validates the given argument is at or above the provided minimum.
   *
   * @param num the argument to validate
   * @param min the minimum value the argument must be at or above
   * @param name the name of the argument
   */
  public static void min(final BigDecimal num, final BigDecimal min, final String name) {
    notBlank(name, "name");
    notNull(num, "num");
    notNull(min, "min");
    if (num.compareTo(min) < 0) {
      throw new IllegalArgumentException(String.format(MIN_MSG_F, name, min));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final int num, final int max, final String name) {
    notBlank(name, "name");
    if (num > max) {
      throw new IllegalArgumentException(String.format(MAX_MSG_D, name, max));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final short num, final short max, final String name) {
    notBlank(name, "name");
    if (num > max) {
      throw new IllegalArgumentException(String.format(MAX_MSG_D, name, max));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final long num, final long max, final String name) {
    notBlank(name, "name");
    if (num > max) {
      throw new IllegalArgumentException(String.format(MAX_MSG_D, name, max));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final float num, final float max, final String name) {
    notBlank(name, "name");
    if (num > max) {
      throw new IllegalArgumentException(String.format(MAX_MSG_F, name, max));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final double num, final double max, final String name) {
    notBlank(name, "name");
    if (num > max) {
      throw new IllegalArgumentException(String.format(MAX_MSG_F, name, max));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final BigInteger num, final BigInteger max, final String name) {
    notBlank(name, "name");
    notNull(num, "num");
    notNull(max, "max");
    if (num.compareTo(max) > 0) {
      throw new IllegalArgumentException(String.format(MAX_MSG_D, name, max));
    }
  }

  /**
   * Validates the given argument is at or below the provided maximum.
   *
   * @param num the argument to validate
   * @param max the maximum value the argument must be at or below
   * @param name the name of the argument
   */
  public static void max(final BigDecimal num, final BigDecimal max, final String name) {
    notBlank(name, "name");
    notNull(num, "num");
    notNull(max, "max");
    if (num.compareTo(max) > 0) {
      throw new IllegalArgumentException(String.format(MAX_MSG_F, name, max));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final int num, final int low, final int high, final String name) {
    notBlank(name, "name");
    if (num < low || num > high) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_D, name, low, high));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final short num, final short low, final short high, final String name) {
    notBlank(name, "name");
    if (num < low || num > high) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_D, name, low, high));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final long num, final long low, final long high, final String name) {
    notBlank(name, "name");
    if (num < low || num > high) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_D, name, low, high));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final float num, final float low, final float high, final String name) {
    notBlank(name, "name");
    if (num < low || num > high) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_F, name, low, high));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final double num, final double low, final double high, final String name) {
    notBlank(name, "name");
    if (num < low || num > high) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_F, name, low, high));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final BigInteger num, final BigInteger low, final BigInteger high, final String name) {
    notBlank(name, "name");
    notNull(num, "num");
    notNull(low, "low");
    notNull(high, "high");
    if (num.compareTo(low) < 0 || num.compareTo(high) > 0) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_D, name, low, high));
    }
  }

  /**
   * Validates the given argument is above the provided minimum and below the provided maximum.
   *
   * @param num the argument to validate
   * @param low the minimum value the argument must be above
   * @param high the maximum  value the argument must be below
   * @param name the name of the argument
   */
  public static void between(final BigDecimal num, final BigDecimal low, final BigDecimal high, final String name) {
    notBlank(name, "name");
    notNull(num, "num");
    notNull(low, "low");
    notNull(high, "high");
    if (num.compareTo(low) < 0 || num.compareTo(high) > 0) {
      throw new IllegalArgumentException(String.format(RANGE_MSG_F, name, low, high));
    }
  }

  /**
   * Validates the given argument matches the provided regular expression.
   *
   * @param val the argument to validate
   * @param regex the regular expression to validate against
   * @param name the name of the argument
   */
  public static void regex(final String val, final String regex, final String name) {
    notBlank(name, "name");
    if (!Pattern.matches(regex, val)) {
      throw new IllegalArgumentException(String.format(REGEX, name, regex));
    }
  }

  /**
   * Validates the given argument matches the provided regular expression.
   *
   * @param val the argument to validate
   * @param regex the regular expression to validate against
   * @param name the name of the argument
   */
  public static void regex(final String val, final Pattern regex, final String name) {
    notBlank(name, "name");
    if (!regex.matcher(val).matches()) {
      throw new IllegalArgumentException(String.format(REGEX, name, regex));
    }
  }

}
