/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper methods for use with the reflection API. Noe that the methods in this class wrap all
 * checked exceptions with their equivalent RuntimeException class. The javadoc for each method
 * should list out the possible exception types that may be thrown by the method.
 *
 * @author Erik R. Jensen
 */
public class ReflectionHelper {

  private static Map<Class<?>, Class<?>> assignable;

  static {
    assignable = new HashMap<Class<?>, Class<?>>(16);
    assignable.put(Boolean.TYPE, Boolean.class);
    assignable.put(Boolean.class, Boolean.TYPE);
    assignable.put(Byte.TYPE, Byte.class);
    assignable.put(Byte.class, Byte.TYPE);
    assignable.put(Short.TYPE, Short.class);
    assignable.put(Short.class, Short.TYPE);
    assignable.put(Integer.TYPE, Integer.class);
    assignable.put(Integer.class, Integer.TYPE);
    assignable.put(Long.TYPE, Long.class);
    assignable.put(Long.class, Long.TYPE);
    assignable.put(Float.TYPE, Float.class);
    assignable.put(Float.class, Float.TYPE);
    assignable.put(Double.TYPE, Double.class);
    assignable.put(Double.class, Double.TYPE);
    assignable.put(Character.TYPE, Character.class);
    assignable.put(Character.class, Character.TYPE);
  }

  /**
   * Creates a new instance of a class using any provided arguments as constructor arguments.
   * This method will first try and find a constructor that exactly matches the given argument
   * types. If it cannot, it will then find the first constructor that can be used with the
   * given arguments.
   *
   * @param clazz the class to create an instance of
   * @param args the constructor arguments
   * @param <T> the type
   * @return the instantiated instance
   * @throws NoSuchMethodRuntimeException if a suitable constructor could not be found
   * @throws InstantiationRuntimeException if the type could not be instantiated
   * @throws IllegalAccessRuntimeException if the constructor could not be accessed
   * @throws InvocationTargetRuntimeException if the constructor could not be called
   */
  @SuppressWarnings("unchecked")
  public static <T> T newInstance(Class<T> clazz, Object... args) {
    try {
      if (args.length == 0) {
        return clazz.newInstance();
      } else {
        final Class<?>[] argTypes = new Class<?>[args.length];
        for (int i = 0; i < args.length; i++) {
          argTypes[i] = args[i].getClass();
        }
        try {
          Constructor<T> c = clazz.getConstructor(argTypes);
          return c.newInstance(args);
        } catch (NoSuchMethodException x) {
          for (Constructor<?> c : clazz.getConstructors()) {
            if (isAssignable(argTypes, c.getParameterTypes())) {
              return (T) c.newInstance(args);
            }
          }
        }
      }
    } catch (Exception x) {
      handle(x);
    }
    throw new NoSuchMethodRuntimeException(
        "Could not find public constructor for the given argument types.");
  }

  /**
   * Finds a class by name.
   *
   * @param name the name of the class to find
   * @return the class or null if not found
   */
  public static Class<?> findClass(String name) {
    try {
      return Class.forName(name);
    } catch (ClassNotFoundException x) {
      return null;
    }
  }

  /**
   * Finds a field by name. The search is performed up the class hierarchy and includes all public,
   * protected, default (package) and private fields which include both static and instance members.
   *
   * @param clazz the class to search in
   * @param field the field to search for
   * @return the field or null if not found
   */
  public static Field findField(Class<?> clazz, final String field) {
    do {
      for (Field f : clazz.getDeclaredFields()) {
        if (f.getName().equals(field)) {
          return f;
        }
      }
    } while ((clazz = clazz.getSuperclass()) != null);
    return null;
  }

  /**
   * Finds a field by name and type. The search is performed up the class hierarchy and includes
   * all public, protected, default (package) and private fields which include both static and
   * instance members.
   *
   * @param clazz the class to search in
   * @param field te field to search for
   * @param type the type of the field
   * @return the field or null if not found
   */
  public static Field findField(Class<?> clazz, final String field, final Class<?> type) {
    do {
      for (Field f : clazz.getDeclaredFields()) {
        if (f.getName().equals(field) && f.getType().equals(type)) {
          return f;
        }
      }
    } while ((clazz = clazz.getSuperclass()) != null);
    return null;
  }

  /**
   * Finds a method by name. The search is performed up the class hierarchy and includes all
   * public,protected, default (package) and private methods which include both static and
   * instance members.
   *
   * @param clazz the class to search in
   * @param method the method to search for
   * @return the method or null if not found
   */
  public static Method findMethod(Class<?> clazz, final String method) {
    do {
      for (Method m : clazz.getDeclaredMethods()) {
        if (m.getName().equals(method)) {
          return m;
        }
      }
    } while ((clazz = clazz.getSuperclass()) != null);
    return null;
  }

  /**
   * Finds a method by name and argument types. The search is performed up the class hierarchy and
   * includes all public, protected, default (package) and private methods which include both
   * static and instance members.
   *
   * @param clazz the class to search in
   * @param method the method to search for
   * @param argTypes the argument types to search for
   * @return the method or null if not found
   */
  public static Method findMethod(Class<?> clazz, final String method, final Class<?>... argTypes) {
    do {
      for (Method m : clazz.getDeclaredMethods()) {
        if (m.getName().equals(method) && ArrayHelper.equals(argTypes, m.getParameterTypes())) {
          return m;
        }
      }
    } while ((clazz = clazz.getSuperclass()) != null);
    return null;
  }

  /**
   * Determines if the type from is assignable to the type to.
   *
   * @param from the type of the source
   * @param to the type of the destination
   * @return true if assignable, false if otherwise
   */
  public static boolean isAssignable(final Class<?> from, final Class<?> to) {
    assert from != null;
    assert to != null;
    if (to.isAssignableFrom(from)) {
      return true;
    } else {
      final Class<?> clazz = assignable.get(from);
      return clazz.equals(to);
    }
  }

  /**
   * Determines if the types in c1 are assignable to the types in c2.
   *
   * @param c1 the array of types
   * @param c2 the array of types
   * @return true if the types in c1 are assignable to the types in c2
   */
  private static boolean isAssignable(final Class<?>[] c1, final Class<?>[] c2) {
    if (c1.length == c2.length) {
      for (int i = c1.length - 1; i >= 0; i--) {
        if (!c2[i].isAssignableFrom(c1[i])) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Gets the value for a field.
   *
   * @param o the object the field is on
   * @param fieldName the name of the field
   * @return the field value
   * @throws NoSuchFieldRuntimeException if the field could not be found
   */
  public static Object getField(final Object o, final String fieldName) {
    // TODO Get private fields
    try {
      return o.getClass().getField(fieldName).get(o);
    } catch (Exception x) {
      handle(x);
    }
    throw new AssertionError("Should never execute.");
  }

  /**
   * Sets the value for a field.
   *
   * @param o the object the field is on
   * @param fieldName the name of the field
   * @param value the value to set
   * @throws NoSuchFieldRuntimeException if the field could not be found
   */
  public static void setField(final Object o, final String fieldName, final Object value) {
    // TODO Set private fields
    try {
      o.getClass().getField(fieldName).set(o, value);
    } catch (Exception x) {
      handle(x);
    }
  }

  /**
   * Converts a field name into a getter method name.
   *
   * @param field the field name to convert
   * @return the getter method name
   */
  public static String toGetter(final String field) {
    return new StringBuilder("get").append(Character.toUpperCase(field.charAt(0)))
        .append(field.substring(1, field.length())).toString();
  }

  /**
   * Converts a field name into a setter method name.
   *
   * @param field the field name to convert
   * @return the setter method name
   */
  public static String toSetter(final String field) {
    return new StringBuilder("set").append(Character.toUpperCase(field.charAt(0)))
        .append(field.substring(1, field.length())).toString();
  }

  /**
   * Invokes a getter method following the JavaBean convention.
   *
   * @param o the object to invoke the getter on
   * @param fieldName the name of the field
   * @return the value returned from the getter
   * @throws NoSuchMethodRuntimeException if the method cannot be found
   * @throws IllegalAccessRuntimeException if the method is not accessible
   * @throws InvocationTargetRuntimeException if there is an error invoking the method
   */
  public static Object invokeGetter(final Object o, final String fieldName) {
    try {
      final Method m = o.getClass().getMethod(toGetter(fieldName));
      return m.invoke(o);
    } catch (Exception x) {
      handle(x);
    }
    throw new AssertionError("Should never execute.");
  }

  /**
   * Invokes a getter method following the JavaBean convention.
   *
   * @param o the object to invoke the getter on
   * @param field the field
   * @return the value returned from the getter
   * @throws NoSuchMethodRuntimeException if the method cannot be found
   * @throws IllegalAccessRuntimeException if the method is not accessible
   * @throws InvocationTargetRuntimeException if there is an error invoking the method
   */
  public static Object invokeGetter(final Object o, final Field field) {
    return invokeGetter(o, field.getName());
  }

  /**
   * Invokes a setter method following the JavaBean convention.
   *
   * @param o the object to invoke the setter on
   * @param fieldName the name of the field
   * @param arg the value to set
   * @throws NoSuchMethodRuntimeException if the method cannot be found
   * @throws IllegalAccessRuntimeException if the method is not accessible
   * @throws InvocationTargetRuntimeException if there is an error invoking the method
   */
  public static void invokeSetter(final Object o, final String fieldName, final Object arg) {
    try {
      final Method m = o.getClass().getMethod(toSetter(fieldName), arg.getClass());
      m.invoke(o, arg);
    } catch (Exception x) {
      handle(x);
    }
  }

  /**
   * Invokes a setter method following the JavaBean convention.
   *
   * @param o the object to invoke the setter on
   * @param field the field
   * @param arg the value to set
   * @throws NoSuchMethodRuntimeException if the method cannot be found
   * @throws IllegalAccessRuntimeException if the method is not accessible
   * @throws InvocationTargetRuntimeException if there is an error invoking the method
   */
  public static void invokeSetter(final Object o, final Field field, final Object arg) {
    invokeSetter(o, field.getName(), arg);
  }

  /**
   * Handles reflection related exceptions.
   *
   * @param x the exception to handle
   */
  private static void handle(final Exception x) {
    if (x instanceof NoSuchMethodException) {
      throw new NoSuchMethodRuntimeException("Method not found: " + x.getMessage(), x);
    } else if (x instanceof NoSuchFieldException) {
      throw new NoSuchFieldRuntimeException("Field not found: " + x.getMessage(), x);
    } else if (x instanceof IllegalAccessException) {
      throw new IllegalAccessRuntimeException("Method is not accessible: " + x.getMessage(), x);
    } else if (x instanceof InvocationTargetException) {
      throw new InvocationTargetRuntimeException("Failed to invoke method: " + x.getMessage(), x);
    } else if (x instanceof RuntimeException) {
      throw (RuntimeException) x;
    } else {
      throw new UnexpectedException("Unexpected exception: " + x.getMessage(), x);
    }
  }
}
