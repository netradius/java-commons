/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

/**
 * This class contains helper methods for working with Strings.
 *
 * @author Erik R. Jensen
 */
public class StringHelper {

  private static String underscoreTo(final CharSequence cs, final boolean pascal) {
    final StringBuilder sb = new StringBuilder();
    boolean underscore = false;
    for (int i = 0; i < cs.length(); i++) {
      char c = cs.charAt(i);
      if (c == '_') {
        underscore = true;
      } else {
        if (underscore || (i == 0 && pascal)) {
          sb.append(Character.toUpperCase(c));
        } else {
          sb.append(c);
        }
        underscore = false;
      }
    }
    return sb.toString();
  }

  /**
   * Converts a string like "thisIsAVariable" to "this_is_a_variable".
   *
   * @param cs the character sequence to convert
   * @return the converted string
   */
  public static String camelCaseToUnderscore(final CharSequence cs) {
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < cs.length(); i++) {
      char c = cs.charAt(i);
      if (Character.isUpperCase(c)) {
        if (i != 0) {
          sb.append("_");
        }
        sb.append(Character.toLowerCase(c));
      } else {
        sb.append(c);
      }
    }
    return sb.toString();
  }

  /**
   * Converts a string like "ThisIsAVariable" to "this_is_a_variable".
   *
   * @param cs the character sequence to convert
   * @return the converted string
   */
  public static String pascalCaseToUnderscore(final CharSequence cs) {
    return camelCaseToUnderscore(cs);
  }

  /**
   * Converts a string like "this_is_a_variable" to "thisIsAVariable".
   *
   * @param cs the character sequence to convert
   * @return the converted string
   */
  public static String underscoreToCamelCase(final CharSequence cs) {
    return underscoreTo(cs, false);
  }

  /**
   * Converts a string like "this_is_a_variable" to "ThisIsAVariable".
   *
   * @param cs the character sequence to convert
   * @return the converted string
   */
  public static String underscoreToPascalCase(final CharSequence cs) {
    return underscoreTo(cs, true);
  }

  /**
   * Checks if a string is null or empty (""). This method does not trim whitespace characters. Please use
   * isBlank for that.
   *
   * @param cs the string to check
   * @return true if the string is empty, false if otherwise
   */
  public static boolean isEmpty(final CharSequence cs) {
    return cs == null || cs.length() ==  0;
  }

  /**
   * Checks if a string is not null and not empty (""). This method does not trim whitespace characters. Please
   * use isNotBlank for that.
   *
   * @param cs the string to check
   * @return true if the string is not empty, false if otherwise
   */
  public static boolean isNotEmpty(final CharSequence cs) {
    return !isEmpty(cs);
  }

  /**
   * Checks if a string is null and contains no non-whitespace characters.
   *
   * @param cs the string to check
   * @return true if the string is blank, false if otherwise
   */
  public static boolean isBlank(final CharSequence cs) {
    return cs == null || trimWhitespace(cs).isEmpty();
  }

  /**
   * Checks if a string is not null or contains only non-whitespace characters.
   *
   * @param cs the string to check
   * @return true if the string is not blank, false if otherwise
   */
  public static boolean isNotBlank(final CharSequence cs) {
    return !isBlank(cs);
  }

  /**
   * Trims off any leading or trailing whitespace from the given string. This method accepts null and empty arguments.
   *
   * @param cs the string to trim
   * @return the trimmed string
   */
  public static String trimWhitespace(final CharSequence cs) {
    return trimLeadingWhitespace(trimTrailingWhitespace(cs));
  }

  /**
   * Trims off any leading whitespace from the given string. This method accepts null and empty arguments.
   *
   * @param cs the string to trim
   * @return the trimmed string
   */
  public static String trimLeadingWhitespace(final CharSequence cs) {
    if (cs == null) {
      return null;
    }
    final StringBuilder sb = new StringBuilder(cs.length());
    for (int i = 0; i < cs.length(); i++) {
      if (!Character.isWhitespace(cs.charAt(i))) {
        sb.append(cs.subSequence(i, cs.length()));
        break;
      }
    }
    return sb.toString();
  }

  /**
   * Trims off any training whitespace from the given string. This method accepts null and empty arguments.
   *
   * @param cs the string to trim
   * @return the trimmed string
   */
  public static String trimTrailingWhitespace(final CharSequence cs) {
    if (cs == null) {
      return null;
    }
    final StringBuilder sb = new StringBuilder(cs.length());
    for (int i = cs.length() - 1; i >= 0; i--) {
      if (!Character.isWhitespace(cs.charAt(i))) {
        sb.append(cs.subSequence(0, i + 1));
        break;
      }
    }
    return sb.toString();
  }

}
