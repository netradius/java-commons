/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles converting primitive types to wrapper types.
 *
 * @author Erik R. Jensen
 */
public class PrimitiveToWrapperConverter implements Converter {

  private static final Map<Class<?>, Class<?>> map;

  static {
    map = new HashMap<Class<?>, Class<?>>(8);
    map.put(Boolean.class, Boolean.TYPE);
    map.put(Byte.class, Byte.TYPE);
    map.put(Short.class, Short.TYPE);
    map.put(Integer.class, Integer.TYPE);
    map.put(Long.class, Long.TYPE);
    map.put(Float.class, Float.TYPE);
    map.put(Double.class, Double.TYPE);
    map.put(Character.class, Character.TYPE);
  }

  public boolean converts(Class<?> from, Class<?> to) {
    final Class<?> clazz = map.get(from);
    return clazz != null && clazz.equals(to);
  }

  public Object convert(Object o, Class<?> to) {
    return null;
  }
}
