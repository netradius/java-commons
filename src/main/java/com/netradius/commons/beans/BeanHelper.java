/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.beans;

import com.netradius.commons.lang.IllegalAccessRuntimeException;
import com.netradius.commons.lang.InvocationTargetRuntimeException;
import com.netradius.commons.lang.NoSuchMethodRuntimeException;
import com.netradius.commons.lang.ReflectionHelper;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * Common helper methods for working with JavaBeans.
 *
 * @author Erik R. Jensen
 */
public class BeanHelper {

  /**
   * Gets the value of a property on the given object.
   *
   * @param o the object
   * @param property the name of the property
   * @return the value of the property
   * @throws IntrospectionRuntimeException if there is an error introspecting the class
   * @throws IllegalAccessRuntimeException if an error occurs accessing the read method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the read method
   * @throws NoSuchMethodRuntimeException if the read method could not be found
   */
  public static Object getPropertyValue(final Object o, final String property) {
    return getPropertyValue(o, getPropertyDescriptor(o.getClass(), property));
  }

  /**
   * Gets the value of a property on the given object.
   *
   * @param o the object
   * @param descriptor the PropertyDescriptor that defines the property
   * @return the value of the property
   * @throws IllegalAccessRuntimeException if an error occurs accessing the read method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the read method
   * @throws NoSuchMethodRuntimeException if the read method could not be found
   */
  public static Object getPropertyValue(final Object o, final PropertyDescriptor descriptor) {
    final Method m = descriptor.getReadMethod();
    if (m != null) {
      try {
        return m.invoke(o);
      } catch (IllegalAccessException x) {
        throw new IllegalAccessRuntimeException(x);
      } catch (InvocationTargetException x) {
        throw new InvocationTargetRuntimeException(x);
      }
    }
    throw new NoSuchMethodRuntimeException("No read method for property "
        + descriptor.getName() + " on class " + o.getClass().getName());
  }

  /**
   * Sets the value of a property on the given object.
   *
   * @param o the object
   * @param property the name of the property
   * @param value the value to be set
   * @throws IntrospectionRuntimeException if there is an error introspecting the class
   * @throws IllegalAccessRuntimeException if an error occurs accessing the write method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the write method
   * @throws NoSuchMethodRuntimeException if the write method could not be found
   */
  public static void setPropertyValue(final Object o, final String property, final Object value) {
    setPropertyValue(o, getPropertyDescriptor(o.getClass(), property), value);
  }

  /**
   * Sets the value of a property on the given object.
   *
   * @param o the object
   * @param descriptor the PropertyDescriptor that defines tht property
   * @param value the value to be set
   * @throws IllegalAccessRuntimeException if an error occurs accessing the write method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the write method
   * @throws NoSuchMethodRuntimeException if the write method could not be found
   */
  public static void setPropertyValue(final Object o, final PropertyDescriptor descriptor,
      final Object value) {
    final Method m = descriptor.getWriteMethod();
    if (m != null) {
      try {
        m.invoke(o, value);
        return;
      } catch (IllegalAccessException x) {
        throw new IllegalAccessRuntimeException(x);
      } catch (InvocationTargetException x) {
        throw new InvocationTargetRuntimeException(x);
      }
    }
    throw new NoSuchMethodRuntimeException("No write method for property "
        + descriptor.getName() + " on class " + o.getClass().getName());
  }

  /**
   * Gets the property descriptor for the given property.
   *
   * @param clazz the class the property is on
   * @param property the name of the property
   * @return the property descriptor or null if not found
   * @throws IntrospectionRuntimeException if there is an error introspecting the class
   */
  public static PropertyDescriptor getPropertyDescriptor(final Class<?> clazz,
      final String property) {
    try {
      BeanInfo info = Introspector.getBeanInfo(clazz);
      // TODO Look into providing a class loader safe way to do caching and benchmark
      for (PropertyDescriptor descriptor : info.getPropertyDescriptors()) {
        if (descriptor.getName().equals(property)) {
          return descriptor;
        }
      }
    } catch (IntrospectionException x) {
      throw new IntrospectionRuntimeException(x);
    }
    return null;
  }

  /**
   * Gets all the property descriptors for the given class.
   *
   * @param clazz the class
   * @return the property descriptors for the class
   * @throws IntrospectionRuntimeException if there is an error introspecting the class
   */
  public static PropertyDescriptor[] getPropertyDescriptors(final Class<?> clazz) {
    try {
      // TODO Look into providing a class loader safe way to do caching and benchmark
      return Introspector.getBeanInfo(clazz).getPropertyDescriptors();
    } catch (IntrospectionException x) {
      throw new IntrospectionRuntimeException(x);
    }
  }

  /**
   * Copies all properties from the source to the target where property names are the same.
   *
   * @param source the source
   * @param target the target
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyProperties(final Object source, final T target,
      final String... ignoreProperties) {
    return copyProperties(source, target, false, new Converter[]{},
        Arrays.asList(ignoreProperties));
  }

  /**
   * Copies all properties from the source to the target where property names are the same.
   *
   * @param source the source
   * @param target the target
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyProperties(final Object source, final T target,
      final Collection<String> ignoreProperties) {
    return copyProperties(source, target, false, new Converter[]{}, ignoreProperties);
  }

  /**
   * Copies all properties from the source to the target where property names are the same,
   * applying any provided type converters during the copying.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyProperties(Object source, T target, Converter[] converters,
      String... ignoreProperties) {
    return copyProperties(source, target, false, converters, Arrays.asList(ignoreProperties));
  }

  /**
   * Copies all properties from the source to the target where property names are the same,
   * applying any provided type converters during the copying.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyProperties(Object source, T target, Converter[] converters,
      Collection<String> ignoreProperties) {
    return copyProperties(source, target, false, converters, ignoreProperties);
  }

  private static <T> T copyProperties(final Object source, T target, final boolean filterNull,
      final Converter[] converters, final Collection<String> ignoreProperties) {
    final HashSet<String> ignore = new HashSet<String>(ignoreProperties.size());
    ignore.addAll(ignoreProperties);
    for (PropertyDescriptor src : getPropertyDescriptors(source.getClass())) {
      if (!src.getName().equals("class") && !ignore.contains(src.getName())) {
        copyProperty(source, src, target, filterNull, converters);
      }
    }
    return target;
  }

  /**
   * Copies all properties from the source to the target where names are the same and the values
   * are not null.
   *
   * @param source the source
   * @param target the target
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullProperties(Object source, T target, String... ignoreProperties) {
    return copyProperties(source, target, true, new Converter[]{}, Arrays.asList(ignoreProperties));
  }

  /**
   * Copies all properties from the source to the target where names are the same and the values
   * are not null.
   *
   * @param source the source
   * @param target the target
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullProperties(Object source, T target,
      Collection<String> ignoreProperties) {
    return copyProperties(source, target, true, new Converter[]{}, ignoreProperties);
  }

  /**
   * Copies all properties from the source to the target where names are the same and the values
   * are not null, applying any type converters during the copying.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullProperties(Object source, T target, Converter[] converters,
      String... ignoreProperties) {
    return copyProperties(source, target, true, converters, Arrays.asList(ignoreProperties));
  }

  /**
   * Copies all properties from the source to the target where names are the same and the values
   * are not null, applying any type converters during the copying.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param ignoreProperties the property names to ignore
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullProperties(Object source, T target, Converter[] converters,
      Collection<String> ignoreProperties) {
    return copyProperties(source, target, true, converters, ignoreProperties);
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target.
   *
   * @param source the source
   * @param target the target
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyPropertiesByName(Object source, T target, String... properties) {
    return copyPropertiesByName(source, target, false, null, Arrays.asList(properties));
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target.
   *
   * @param source the source
   * @param target the target
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyPropertiesByName(Object source, T target, Collection<String> properties) {
    return copyPropertiesByName(source, target, false, null, properties);
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyPropertiesByName(Object source, T target, Converter[] converters,
      String... properties) {
    return copyPropertiesByName(source, target, false, converters, Arrays.asList(properties));
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyPropertiesByName(Object source, T target, Converter[] converters,
      Collection<String> properties) {
    return copyPropertiesByName(source, target, false, converters, properties);
  }

  private static <T> T copyPropertiesByName(final Object source, T target, final boolean filterNull,
      final Converter[] converters, final Collection<String> properties) {
    for (String p : properties) {
      final PropertyDescriptor src = getPropertyDescriptor(source.getClass(), p);
      if (src != null) {
        copyProperty(source, src, target, filterNull, converters);
      }
    }
    return target;
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target and the value of the property on the source is not null.
   *
   * @param source the source
   * @param target the target
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullPropertiesByName(Object source, T target, String... properties) {
    return copyPropertiesByName(source, target, true, null, Arrays.asList(properties));
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target and the value of the property on the source is not null.
   *
   * @param source the source
   * @param target the target
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullPropertiesByName(Object source, T target,
      Collection<String> properties) {
    return copyPropertiesByName(source, target, true, null, properties);
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target and the value of the property on the source is not null.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullPropertiesByName(Object source, T target, Converter[] converters,
      String... properties) {
    return copyPropertiesByName(source, target, true, converters, Arrays.asList(properties));
  }

  /**
   * Copies the specified properties from the source to the target if the property exists
   * on both the source and target and the value of the property on the source is not null.
   *
   * @param source the source
   * @param target the target
   * @param converters the type converters to use
   * @param properties the properties to copy
   * @param <T> the type of the target
   * @return the target for convenience
   * @throws IntrospectionRuntimeException if an error occurs introspecting the source or target
   * @throws IllegalAccessRuntimeException if an error occurs accessing the a method
   * @throws InvocationTargetRuntimeException if an error occurs invoking the a method
   * @throws NoSuchMethodRuntimeException if a method could not be found
   */
  public static <T> T copyNotNullPropertiesByName(Object source, T target, Converter[] converters,
      Collection<String> properties) {
    return copyPropertiesByName(source, target, true, converters, properties);
  }

  private static void copyProperty(final Object source, final PropertyDescriptor src,
      final Object target, final boolean filterNull,
      final Converter[] converters) {
    final PropertyDescriptor dst = getPropertyDescriptor(target.getClass(), src.getName());
    if (dst != null) {
      if (dst.getWriteMethod() == null) { // This is used to deal with chained accessor methods
        String name = dst.getName();
        name = "set" + name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
        Method readMethod = dst.getReadMethod();
        try {
          Method writeMethod = readMethod.getDeclaringClass()
              .getMethod(name, readMethod.getReturnType());
          dst.setWriteMethod(writeMethod);
        } catch (NoSuchMethodException | IntrospectionException x) { /* do nothing */ }
      }
      if (dst.getWriteMethod() != null) {
        if (ReflectionHelper.isAssignable(src.getPropertyType(), dst.getPropertyType())) {
          final Object value = getPropertyValue(source, src);
          if (value != null || !filterNull) {
            setPropertyValue(target, dst, value);
          }
        } else { // Not assignable
          for (Converter converter : converters) {
            if (converter.converts(src.getPropertyType(), dst.getPropertyType())) {
              final Object value = getPropertyValue(source, src);
              setPropertyValue(target, dst, converter.convert(value, dst.getPropertyType()));
              return;
            }
          }
          throw new IllegalArgumentException("Type mismatch on property " + dst.getName()
              + ". Cannot convert " + src.getPropertyType().getName() + " to "
              + dst.getPropertyType().getName() + ".");
        }
      }
    }
  }
}
