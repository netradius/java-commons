/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.digest;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ValidationHelper;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This file contains common helper methods for working with message digests.
 */
public class DigestHelper {

  public static final String MD2 = "MD2";
  public static final String MD5 = "MD5";
  public static final String SHA1 = "SHA";
  public static final String SHA224 = "SHA-224";
  public static final String SHA256 = "SHA-256";
  public static final String SHS512 = "SHA-512";

  private static final String ALGORITHM_NOT_FOUND = "Algorithm %s was not found";

  public static Set<String> getAlgorithms() {
    return Arrays.stream(Security.getProviders())
        .flatMap(p -> p.getServices().stream()
            .filter(s -> s.getType().equalsIgnoreCase("MessageDigest"))
            .map(Provider.Service::getAlgorithm))
        .collect(Collectors.toSet());
  }

  public static String digest(final String algorithm, final File file) throws IOException {
    return BitTwiddler.tohexstr(digestb(algorithm, file), true);
  }

  public static byte[] digestb(final String algorithm, final File file) throws IOException {
    ValidationHelper.notBlank(algorithm, "algorithm");
    try {
      MessageDigest md = MessageDigest.getInstance(algorithm);
      try (RandomAccessFile afile = new RandomAccessFile(file, "r");
           FileChannel fc = afile.getChannel()) {
        MappedByteBuffer mb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
        mb.load();
        md.update(mb);
        return md.digest();
      }
    } catch (NoSuchAlgorithmException x) {
      throw new IllegalArgumentException(String.format(ALGORITHM_NOT_FOUND, algorithm));
    }
  }

  public static String sha1(final File file) throws IOException {
    return digest(SHA1, file);
  }

  public static byte[] sha1b(final File file) throws IOException {
    return digestb(SHA1, file);
  }

  public static String sha256(final File file) throws IOException {
    return digest(SHA256, file);
  }

  public static byte[] sha256b(final File file) throws IOException {
    return digestb(SHA256, file);
  }

  public static String sha512(final File file) throws IOException {
    return digest(SHS512, file);
  }

  public static byte[] sha512b(final File file) throws IOException {
    return digestb(SHS512, file);
  }

}
