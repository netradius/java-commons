/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Helper methods for dealing with random number generation.
 */
public class RandomHelper {

  private static final int PAGE_SIZE = 1024;

  /**
   * Fills the given ByteBuffer with random data.
   *
   * @param random the random source to use
   * @param buf the buffer to fill
   */
  public static void rbuf(final Random random, final ByteBuffer buf) {
    byte[] b = new byte[PAGE_SIZE];
    while (buf.hasRemaining()) {
      if (random == null) {
        ThreadLocalRandom.current().nextBytes(b);
      } else {
        random.nextBytes(b);
      }
      buf.put(b, 0, buf.remaining() < PAGE_SIZE ? buf.remaining() : b.length);
    }
  }

  /**
   * Fills the given ByteBuffer with random data.
   *
   * @param buf the buffer to fill
   */
  public static void rbuf(final ByteBuffer buf) {
    rbuf(null, buf);
  }

  /**
   * Writes random data to the given file. If the file does not exist, it will be created.
   *
   * @param random the random source to use
   * @param file the file to write to
   * @param length the number of bytes to write
   * @throws IOException if an I/O error occurs
   */
  public static void rfile(final Random random, final File file, long length) throws IOException {
    try (RandomAccessFile rfile = new RandomAccessFile(file, "rw");
         FileChannel fc = rfile.getChannel()) {
      ByteBuffer buf = ByteBuffer.allocate(PAGE_SIZE);
      while (length > 0) {
       rbuf(random, buf);
        buf.flip();
        if (length < PAGE_SIZE) {
          buf.position((int)(PAGE_SIZE - length));
        }
        fc.write(buf);
        length -= PAGE_SIZE;
        buf.clear();
      }
    }
  }

  /**
   * Writes random data to the given file. If the file does not exist, it will be created.
   *
   * @param file the file to write to
   * @param length the number of bytes to write
   * @throws IOException if an I/O error occurs
   */
  public static void rfile(final File file, long length) throws IOException  {
    rfile(null, file, length);
  }
}
