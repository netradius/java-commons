/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
    
package com.netradius.commons.io;

import com.netradius.commons.bitsnbytes.BitTwiddler;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.util.List;

/**
 * This class contains common helper methods for I/O.
 *
 * @author Erik R. Jensen
 */
public class IOHelper {

  /**
   * Default buffer size used when no buffer sizes is provided. Default is 16KB.
   */
  public static final int DEFAULT_BUFFER_SIZE = 16 * 1024; // 16KB

  /**
   * Quietly closes a connection. This method is equivalent to {@link java.io.Closeable#close()}
   * except IOExceptions will be ignored. Unlike org.apache.commons.io.IOUtils#close(Closeable),
   * this method only catches IOExceptions, not all exceptions.
   *
   * @param closeables the the objects to be closed which may be null or already closed
   */
  public static void close(Closeable... closeables) {
    for (Closeable c : closeables) {
      if (c != null) {
        try {
          c.close();
        } catch (IOException x) {
          // ignore
        }
      }
    }
  }

  public static long copy(final InputStream in, final OutputStream out) throws IOException {
    return copy(in, out, -1, DEFAULT_BUFFER_SIZE);
  }

  public static long copy(final InputStream in, final OutputStream out, final int bufferSize)
      throws IOException {
    return copy(in, out, -1, bufferSize);
  }

  public static long copy(final InputStream in, final OutputStream out, final long numBytes)
      throws IOException {
    return copy(in, out, numBytes, DEFAULT_BUFFER_SIZE);
  }

  public static long copy(final InputStream in, final OutputStream out, final long numBytes,
      final int bufferSize) throws IOException {
    return copy(Channels.newChannel(in), Channels.newChannel(out), numBytes, bufferSize);
  }

  public static long copy(final InputStream in, final WritableByteChannel dest)
      throws IOException {
    return copy(Channels.newChannel(in), dest, -1, DEFAULT_BUFFER_SIZE);
  }

  public static long copy(final InputStream in, final WritableByteChannel dest,
      final int bufferSize) throws IOException {
    return copy(Channels.newChannel(in), dest, -1, bufferSize);
  }

  public static long copy(final InputStream in, final WritableByteChannel dest,
      final long numBytes) throws IOException {
    return copy(Channels.newChannel(in), dest, numBytes, DEFAULT_BUFFER_SIZE);
  }

  public static long copy(final ReadableByteChannel src, final WritableByteChannel dest)
      throws IOException {
    return copy(src, dest, -1, DEFAULT_BUFFER_SIZE);
  }

  public static long copy(final ReadableByteChannel src, final WritableByteChannel dest,
      final int bufferSize) throws IOException {
    return copy(src, dest, -1, bufferSize);
  }

  public static long copy(final ReadableByteChannel src, final WritableByteChannel dest,
      final long numBytes) throws IOException {
    return copy(src, dest, numBytes, DEFAULT_BUFFER_SIZE);
  }

  public static long copy(final ReadableByteChannel src, final WritableByteChannel dest,
      final long numBytes, final int bufferSize) throws IOException {
    final ByteBuffer buf = ByteBuffer.allocate(
        bufferSize < numBytes || numBytes == -1
            ? bufferSize
            : (int) numBytes);
    long count = 0;
    long remain = -1;
    int read;
    while ((read = src.read(buf)) != -1 && remain != 0) {
      count += read;
      assert numBytes == -1 || count <= numBytes;
      buf.flip();
      dest.write(buf);
      buf.compact();
      if (numBytes != -1 && buf.remaining() > (remain = numBytes - count)) {
        buf.limit(buf.position() + (int) remain);
      }
    }
    buf.flip();
    while (buf.hasRemaining()) {
      dest.write(buf);
    }
    return count;
  }

  /**
   * Fills an array from the provided input stream. This method blocks until either len bytes have
   * been read, the end of the stream has been reached, or an exception has been thrown. If the
   * number of bytes read is less than the given length, then the end of the stream has been
   * reached.
   *
   * @param in the input stream used to fill the array
   * @param b the array to fill
   * @param off the offset into the array to begin filling
   * @param len the number of bytes to fill
   * @return the number of bytes read
   * @throws IOException if an I/O error occurs
   */
  public static int fill(final InputStream in, final byte[] b, int off, final int len)
      throws IOException {
    int l = len;
    int r = 0;
    while (l > 0 && r != -1) {
      l -= r;
      off += r;
      r = in.read(b, off, l);
    }
    return len - l;
  }

  /**
   * Fills an array from the provided input stream. This method blocks until either the byte array
   * has been filled, the end of the stream has been reached, or an exception has been thrown. If
   * the number of bytes read is less than the byte array length, then the end of the stream has
   * been reached.
   *
   * @param in the input stream used to fill the array
   * @param b the array to fill
   * @return the number of bytes read
   * @throws IOException if an I/O error occurs
   */
  public static int fill(final InputStream in, final byte[] b) throws IOException {
    return fill(in, b, 0, b.length);
  }

  /**
   * Reads data from an input stream counting the number of bytes.
   *
   * @param src the source to read from
   * @return the number of bytes read
   * @throws IOException if an I/O error occurs
   */
  public static long count(final ReadableByteChannel src) throws IOException {
    final ByteBuffer buf = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
    long count = 0;
    int read = 0;
    while ((read = src.read(buf)) != -1) {
      count += read;
      buf.clear();
    }
    return count;
  }

  /**
   * Reads data from an input stream counting the number of bytes.
   *
   * @param src the source to read from
   * @return the number of bytes read
   * @throws IOException if an I/O error occurs
   */
  public static long count(final InputStream src) throws IOException {
    return count(Channels.newChannel(src));
  }

  /**
   * Returns all offsets where the byte pattern is found in the stream of bytes in the order they
   * were found. This method will search to the end of the stream. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream. This method
   * calls {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(java.io.InputStream, byte[])}
   * and is provided on this class for convenience.
   *
   * @param in the input stream to search
   * @param p the pattern to search for
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final byte[] p) throws IOException {
    return BitTwiddler.search(in, p);
  }

  /**
   * Returns all offsets where the pattern is found in the stream of bytes in the order they were
   * found. This method will search until len bytes have been read. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream. This method
   * calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(java.io.InputStream, long, byte[])}
   * and is provided on this class for convenience.
   *
   * @param in the input stream to search
   * @param len the number of bytes to read
   * @param p the pattern to search for
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final long len, final byte[] p)
      throws IOException {
    return BitTwiddler.search(in, len, p);
  }

  /**
   * Returns all offsets where the pattern is found in the stream of bytes in the order they were
   * found. This method will search until the end of the stream. It is the responsibility of the
   * calling code to close the input stream as this method will NOT close the stream. This method
   * calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(java.io.InputStream, byte[],
   * byte[])}
   * and is provided on this class for convenience.
   *
   * @param in the input stream to search
   * @param p the pattern to search for
   * @param b a buffer to use during searching
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final byte[] p, final byte[] b)
      throws IOException {
    return BitTwiddler.search(in, p, b);
  }

  /**
   * Returns all offsets where the pattern is found in the stream of bytes in the order they were
   * found. This method will search until len bytes have been read. It is the responsibility of
   * the calling code to close the input stream as this method will NOT close the stream.
   * Thismethod calls
   * {@link com.netradius.commons.bitsnbytes.BitTwiddler#search(java.io.InputStream, long, byte[],
   * byte[])} and is provided on this class for convenience.
   *
   * @param in the input stream to search
   * @param len the number of bytes to read
   * @param p the pattern to search for
   * @param b the buffer to use during searching
   * @return the offsets where the pattern was found
   * @throws IOException if an I/O error occurs
   */
  public static List<Long> search(final InputStream in, final long len, final byte[] p,
      final byte[] b)
      throws IOException {
    return BitTwiddler.search(in, len, p, b);
  }

  /**
   * Reads a string from an input stream. This method uses the JVM's default character set. This
   * method will not close the InputStream. It is the responsibility of the calling code to cleanup
   * after itself.
   *
   * @param in the input stream to read
   * @return the string
   * @throws IOException if an I/O error occurs
   */
  public static String readString(final InputStream in) throws IOException {
    return readString(in, Charset.defaultCharset());
  }

  /**
   * Reads a string from an input stream. This method will not close the InputStream. It is the
   * responsibility of the calling code to cleanup after itself.
   *
   * @param in the input stream to read
   * @param encoding the encoding of the string
   * @return the string
   * @throws IOException if an I/O error occurs
   */
  public static String readString(final InputStream in, final String encoding) throws IOException {
    return readString(in, Charset.forName(encoding));
  }

  /**
   * Reads a string from an input stream. This method will not close the InputStream. It is this
   * responsibility of the calling code to cleanup after itself.
   *
   * @param in the input stream to read
   * @param charset the character set of the string
   * @return the string
   * @throws IOException if an I/O error occurs
   */
  public static String readString(final InputStream in, final Charset charset) throws IOException {
    return readString(new InputStreamReader(new BufferedInputStream(in), charset));
  }

  /**
   * Reads a string from a reader. This method will not close the Reader. It is the responsibility
   * of the calling code to cleanup after itself.
   *
   * @param reader the reader to read
   * @return the string
   * @throws IOException if an I/O error occurs
   */
  public static String readString(final Reader reader) throws IOException {
    final StringBuilder sb = new StringBuilder();
    final char[] b = new char[2048]; // Could be anything from 2KB to 8KB in size
    int n;
    while ((n = reader.read(b)) >= 0) {
      sb.append(b, 0, n);
    }
    return sb.toString();
  }
}
