/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Simple InputStream wrapper which can be used to cap the number of bytes which can be read from
 * an InputStream. This wrapper will allow the specified number of bytes to be read from the
 * underlying InputStream. Once the limit has been reached, any subsequent calls to read bytes
 * will throw an CappedInputException.
 *
 * @author Erik R. Jensen
 */
public class CappedInputStream extends CountingInputStream {

  private long cap;

  /**
   * Creates a new CappedInputStream instance.
   *
   * @param in the InputStream to wrap
   * @param cap the maximum number of bytes to allow to be read before throwing CappedInputException
   */
  public CappedInputStream(InputStream in, long cap) {
    super(in);
    this.cap = cap;
  }

  @Override
  public int read() throws IOException {
    if (getCount() >= cap) {
      throw new CappedInputException(cap);
    }
    return super.read();
  }

  @Override
  public int read(byte[] b) throws IOException {
    if (getCount() >= cap) {
      throw new CappedInputException(cap);
    }
    return getCount() + b.length > cap
        ? super.read(b, 0, (int) (cap - getCount()))
        : super.read(b);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    if (getCount() >= cap) {
      throw new CappedInputException(cap);
    }
    return getCount() + len > cap
        ? super.read(b, off, (int) (cap - getCount()))
        : super.read(b, off, len);
  }
}
