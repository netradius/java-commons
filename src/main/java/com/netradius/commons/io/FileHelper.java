/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * This class contains common helper methods for working with files.
 *
 * @author Erik R. Jensen
 */
public class FileHelper {

  /**
   * Deletes a file or a directory including all its contents.
   *
   * @param file the file or directory to delete
   */
  public static void delete(final File file) {
    if (file.isDirectory()) {
      for (File c : file.listFiles()) {
        delete(c);
      }
    }
    file.delete();
  }

  /**
   * Copies a file.
   *
   * @param srcFile the source
   * @param destFile the destination
   * @throws IOException if an I/O error occurs
   */
  public static void copy(final File srcFile, final File destFile) throws IOException {
    if (!destFile.exists()) {
      destFile.createNewFile();
    }
    FileChannel src = null;
    FileChannel dest = null;
    try {
      src = new FileInputStream(srcFile).getChannel();
      dest = new FileOutputStream(destFile).getChannel();
      dest.transferFrom(src, 0, src.size());
    } finally {
      IOHelper.close(src, dest);
    }
  }
}
