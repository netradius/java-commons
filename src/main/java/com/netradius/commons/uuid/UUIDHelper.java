/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.uuid;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;

import java.util.Date;
import java.util.UUID;

/**
 * Helper methods for use with version 1 (time based) UUIDs. This class depends on Johann Burkard's
 * UUID implementation. Some of the functionality in this helper class was adapted from examples in
 * the
 * <a href="http://wiki.apache.org.cassandra/FAQ#working_with_timeuuid_in_java">
 *   Apache Cassandra FAQ
 * </a>.
 *
 * @author Erik R. Jensen
 */
public class UUIDHelper {

  /**
   * The number of 100 nanosecond intervals since the Gregorian calendar was adopted in the west.
   */
  public static final Long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;

  /**
   * The number of bytes in a UUID
   */
  public static final int UUID_BYTE_LENGTH = 16;

  /**
   * Time base generator in a holder class to defer initialization until needed.
   */
  private static class Holder {
    static final TimeBasedGenerator timeBasedGenerator = Generators.timeBasedGenerator();
  }

  /**
   * Generates a new version 1 time based UUID.
   *
   * @return the new version 1 time based UUID
   */
  public static UUID timeUUID() {
    return Holder.timeBasedGenerator.generate();
  }

  /**
   * Generates a new version 4 random UUID.
   *
   * @return the new version 4 random UUID
   */
  public static UUID randomUUID() {
    return UUID.randomUUID();
  }

  /**
   * Returns the version of the UUID.
   *
   * @param uuid the uuid to get the version from
   * @return the UUID version
   */
  public static int getVersion(UUID uuid) {
    byte[] bytes = toBytes(uuid);
    return (bytes[6] & 0x00ff) >>> 4;
  }

  /**
   * Returns the time value from the given time based UUID in 100ns intervals since the Gregorian
   * calendar was adopted in the west or UUID epoch.
   *
   * @param uuid the time based UUID
   * @return the number of 100ns intervals
   */
  public static long getTime100ns(final UUID uuid) {
    final long msb = uuid.getMostSignificantBits();
    long time = msb >> 32 & 0xffffffffL; // low
    time |= (msb << 16) & 0xffff00000000L; // mid
    time |= (msb << 48) & 0xfff000000000000L; // high
    return time;
  }

  /**
   * Returns the time from a time based UUID in milliseconds since January 1, 1970 or epoch.
   *
   * @param uuid the uuid
   * @return the time from the UUID
   */
  public static long getTimeMillis(UUID uuid) {
    return (getTime100ns(uuid) - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000L;
  }

  /**
   * Converts a UUID to a byte array.
   *
   * @param uuid the UUID to convert
   * @return the converted bytes
   */
  public static byte[] toBytes(final UUID uuid) {
    final long msb = uuid.getMostSignificantBits();
    final long lsb = uuid.getLeastSignificantBits();
    final byte[] buffer = new byte[16];
    for (int i = 0; i < 8; i++) {
      buffer[i] = (byte) (msb >>> 8 * (7 - i));
    }
    for (int i = 8; i < 16; i++) {
      buffer[i] = (byte) (lsb >>> 8 * (7 - (i)));
    }
    return buffer;
  }

  /**
   * Converts the 16 byte array to a UUID.
   *
   * @param bytes the bytes to convert
   * @return the converted UUID
   */
  public static UUID toUUID(final byte[] bytes) {
    assert bytes.length == 16;
    long msb = 0;
    long lsb = 0;
    for (int i = 0; i < 8; i++) {
      msb = (msb << 8) | (bytes[i] & 0xff);
    }
    for (int i = 8; i < 16; i++) {
      lsb = (lsb << 8) | (bytes[i] & 0xff);
    }
    return new UUID(msb, lsb);
  }

  /**
   * Converts the 16 byte array to a UUID.
   *
   * @param bytes the bytes to convert
   * @param offset the offset of the byte array the UUID exists
   * @return the converted UUID
   */
  public static UUID toUUID(final byte[] bytes, int offset) {
    assert bytes.length >= 16;
    assert offset + 16 <= bytes.length;
    long msb = 0;
    long lsb = 0;
    for (int i = offset; i < offset + 8; i++) {
      msb = (msb << 8) | (bytes[i] & 0xff);
    }
    for (int i = offset + 8; i < offset + 16; i++) {
      lsb = (lsb << 8) | (bytes[i] & 0xff);
    }
    return new UUID(msb, lsb);
  }

  /**
   * Converts a date to a UUID. Note that this method will NOT create a universally unique
   * identifier. The UUID created from this method should only be used as a marker for a specific
   * time.
   *
   * @param d the date to convert
   * @return the converted UUID
   */
  public static UUID toUUID(final Date d) {
    final long time = d.getTime() * 10000 + NUM_100NS_INTERVALS_SINCE_UUID_EPOCH;
    final long low = time & 0xffffffffL;
    final long mid = time & 0xffff00000000L;
    final long hi = time & 0xfff000000000000L;
    final long upperLong = (low << 32) | (mid >> 16) | (1 << 12) | (hi >> 48);
    return new UUID(upperLong, 0xC000000000000000L);
  }

  /**
   * Converts the string representation of a UUID to a UUID.
   *
   * @param str the string representation of the UUID
   * @return the UUID or null if the string representation is invalid
   */
  public static UUID toUUID(final String str) {
    try {
      return UUID.fromString(str);
    } catch (IllegalArgumentException x) {
      return null;
    }
  }
}
