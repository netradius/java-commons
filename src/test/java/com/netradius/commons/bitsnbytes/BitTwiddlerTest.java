/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.bitsnbytes;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

import static com.netradius.commons.bitsnbytes.BitTwiddler.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for the BitTwiddler class.
 *
 * @author Erik R. Jensen
 */
public class BitTwiddlerTest {

  private static final Charset UTF8 = StandardCharsets.UTF_8;
  private Random rand = new Random(System.currentTimeMillis());

  @Test
  public void testReverse() {
    byte[] buf = {4, 3, 2, 1, 0};
    reverse(buf);
    for (int i = 0; i < buf.length; i++) {
      assertEquals(i, buf[i]);
    }

    buf = new byte[]{3, 2, 1, 0};
    reverse(buf);
    for (int i = 0; i < buf.length; i++) {
      assertEquals(i, buf[i]);
    }

    buf = new byte[]{8, 7, 6, 5, 4, 3, 2, 1, 0};
    reverse(buf, 2, 6);
    byte[] check = new byte[]{8, 7, 3, 4, 5, 6, 2, 1, 0};
    for (int i = 0; i < buf.length; i++) {
      assertEquals(check[i], buf[i]);
    }
  }

  @Test
  public void letolTest() {
    assertEquals(255, letol(new byte[]{-1}));
    assertEquals(65535, letol(new byte[]{-1, -1}));
    assertEquals(-1l, letol(new byte[]{-1, -1, -1, -1, -1, -1, -1, -1}));
    assertEquals(1l, letol(new byte[]{1, 0, 0, 0, 0, 0, 0, 0}));
    assertEquals(9223372036854775807l, letol(new byte[]{-1, -1, -1, -1, -1, -1, -1, 127}));
    assertEquals(1l, letol(new byte[]{-1, -1, 1, 0, 0, 0, 0, 0, 0, 0, -1, -1}, 2, 10));

    assertEquals(255, letol(ByteBuffer.wrap(new byte[]{-1})));
    assertEquals(65535, letol(ByteBuffer.wrap(new byte[]{-1, -1})));
    assertEquals(-1l, letol(ByteBuffer.wrap(new byte[]{-1, -1, -1, -1, -1, -1, -1, -1})));
    assertEquals(1l, letol(ByteBuffer.wrap(new byte[]{1, 0, 0, 0, 0, 0, 0, 0})));
    assertEquals(9223372036854775807l, letol(ByteBuffer.wrap(new byte[]{-1, -1, -1, -1, -1, -1, -1, 127})));
    assertEquals(1l, letol(ByteBuffer.wrap(new byte[]{-1, -1, 1, 0, 0, 0, 0, 0, 0, 0, -1, -1}), 2, 10));
  }

  @Test
  public void letolExceptionTest() {
    assertThrows(IllegalArgumentException.class, () -> letol(new byte[]{1, 1, 1, 1, 1, 1, 1, 1, 1}));
  }

  @Test
  public void letoiTest() {
    assertEquals(255, letoi(new byte[]{-1}));
    assertEquals(65535, letoi(new byte[]{-1, -1}));
    assertEquals(-1, letoi(new byte[]{-1, -1, -1, -1}));
    assertEquals(1, letoi(new byte[]{1, 0, 0, 0}));
    assertEquals(2147483647, letoi(new byte[]{-1, -1, -1, 127}));
    assertEquals(1l, letoi(new byte[]{-1, -1, 1, 0, 0, 0, -1, -1}, 2, 6));

    assertEquals(255, letoi(ByteBuffer.wrap(new byte[]{-1})));
    assertEquals(65535, letoi(ByteBuffer.wrap(new byte[]{-1, -1})));
    assertEquals(-1, letoi(ByteBuffer.wrap(new byte[]{-1, -1, -1, -1})));
    assertEquals(1, letoi(ByteBuffer.wrap(new byte[]{1, 0, 0, 0})));
    assertEquals(2147483647, letoi(ByteBuffer.wrap(new byte[]{-1, -1, -1, 127})));
    assertEquals(1l, letoi(ByteBuffer.wrap(new byte[]{-1, -1, 1, 0, 0, 0, -1, -1}), 2, 6));
  }

  @Test
  public void letoiExceptionTest() {
    assertThrows(IllegalArgumentException.class, () -> letoi(new byte[]{1, 1, 1, 1, 1}));
  }

  @Test
  public void letosTest() {
    assertEquals(255, letos(new byte[]{-1}));
    assertEquals(-1, letos(new byte[]{-1, -1}));
    assertEquals(1, letos(new byte[]{1, 0}));
    assertEquals(32767, letos(new byte[]{-1, 127}));
    assertEquals(1, letos(new byte[]{-1, -1, 1, 0, -1, -1}, 2, 4));

    assertEquals(255, letos(ByteBuffer.wrap(new byte[]{-1})));
    assertEquals(-1, letos(ByteBuffer.wrap(new byte[]{-1, -1})));
    assertEquals(1, letos(ByteBuffer.wrap(new byte[]{1, 0})));
    assertEquals(32767, letos(ByteBuffer.wrap(new byte[]{-1, 127})));
    assertEquals(1, letos(ByteBuffer.wrap(new byte[]{-1, -1, 1, 0, -1, -1}), 2, 4));
  }

  @Test
  public void letosExceptionTest() {
    assertThrows(IllegalArgumentException.class, () -> letos(new byte[]{1, 1, 1}));
  }

  @Test
  public void betolTest() {
    assertEquals(255, betol(new byte[]{-1}));
    assertEquals(65535, betol(new byte[]{-1, -1}));
    assertEquals(-1l, betol(new byte[]{-1, -1, -1, -1, -1, -1, -1, -1}));
    assertEquals(1l, betol(new byte[]{0, 0, 0, 0, 0, 0, 0, 1}));
    assertEquals(9223372036854775807l, betol(new byte[]{127, -1, -1, -1, -1, -1, -1, -1}));
    assertEquals(1l, betol(new byte[]{-1, -1, 0, 0, 0, 0, 0, 0, 0, 1, -1, -1}, 2, 10));

    assertEquals(255, betol(ByteBuffer.wrap(new byte[]{-1})));
    assertEquals(65535, betol(ByteBuffer.wrap(new byte[]{-1, -1})));
    assertEquals(-1l, betol(ByteBuffer.wrap(new byte[]{-1, -1, -1, -1, -1, -1, -1, -1})));
    assertEquals(1l, betol(ByteBuffer.wrap(new byte[]{0, 0, 0, 0, 0, 0, 0, 1})));
    assertEquals(9223372036854775807l, betol(ByteBuffer.wrap(new byte[]{127, -1, -1, -1, -1, -1, -1, -1})));
    assertEquals(1l, betol(ByteBuffer.wrap(new byte[]{-1, -1, 0, 0, 0, 0, 0, 0, 0, 1, -1, -1}), 2, 10));
  }

  @Test
  public void betolExceptionTest() {
    assertThrows(IllegalArgumentException.class, () -> betos(new byte[]{1, 1, 1, 1, 1, 1, 1, 1, 1}));
  }

  @Test
  public void betoiTest() {
    assertEquals(255, betoi(new byte[]{-1}));
    assertEquals(65535, betoi(new byte[]{-1, -1}));
    assertEquals(-1, betoi(new byte[]{-1, -1, -1, -1}));
    assertEquals(1, betoi(new byte[]{0, 0, 0, 1}));
    assertEquals(2147483647, betoi(new byte[]{127, -1, -1, -1}));
    assertEquals(1l, betoi(new byte[]{-1, -1, 0, 0, 0, 1, -1, -1}, 2, 6));

    assertEquals(255, betoi(ByteBuffer.wrap(new byte[]{-1})));
    assertEquals(65535, betoi(ByteBuffer.wrap(new byte[]{-1, -1})));
    assertEquals(-1, betoi(ByteBuffer.wrap(new byte[]{-1, -1, -1, -1})));
    assertEquals(1, betoi(ByteBuffer.wrap(new byte[]{0, 0, 0, 1})));
    assertEquals(2147483647, betoi(ByteBuffer.wrap(new byte[]{127, -1, -1, -1})));
    assertEquals(1l, betoi(ByteBuffer.wrap(new byte[]{-1, -1, 0, 0, 0, 1, -1, -1}), 2, 6));
  }

  @Test
  public void betoiExceptionTest() {
    assertThrows(IllegalArgumentException.class, () -> betoi(new byte[]{1, 1, 1, 1, 1}));
  }

  @Test
  public void betosTest() {
    assertEquals(255, betos(new byte[]{-1}));
    assertEquals(-1, betos(new byte[]{-1, -1}));
    assertEquals(1, betos(new byte[]{0, 1}));
    assertEquals(32767, betos(new byte[]{127, -1}));
    assertEquals(1, betos(new byte[]{-1, -1, 0, 1, -1, -1}, 2, 4));

    assertEquals(255, betos(ByteBuffer.wrap(new byte[]{-1})));
    assertEquals(-1, betos(ByteBuffer.wrap(new byte[]{-1, -1})));
    assertEquals(1, betos(ByteBuffer.wrap(new byte[]{0, 1})));
    assertEquals(32767, betos(ByteBuffer.wrap(new byte[]{127, -1})));
    assertEquals(1, betos(ByteBuffer.wrap(new byte[]{-1, -1, 0, 1, -1, -1}), 2, 4));
  }

  @Test
  public void betosExceptionTest() {
    assertThrows(IllegalArgumentException.class, () -> betos(new byte[]{1, 1, 1}));
  }

  @Test
  public void ltoleTest() {
    assertArrayEquals(new byte[]{-1, 0, 0, 0, 0, 0, 0, 0}, ltole(255l));
    assertArrayEquals(new byte[]{-1, -1, 0, 0, 0, 0, 0, 0}, ltole(65535l));
    assertArrayEquals(new byte[]{-1, -1, -1, -1, 0, 0, 0, 0}, ltole(4294967295l));
    assertArrayEquals(new byte[]{-1, -1, -1, -1, -1, -1, -1, -1}, ltole(-1l));
    assertArrayEquals(new byte[]{1, 0, 0, 0, 0, 0, 0, 0}, ltole(1l));
    assertArrayEquals(new byte[]{-1, -1, -1, -1, -1, -1, -1, 127}, ltole(9223372036854775807l));
  }

  @Test
  public void itoleTest() {
    assertArrayEquals(new byte[]{-1, 0, 0, 0}, itole(255));
    assertArrayEquals(new byte[]{-1, -1, 0, 0}, itole(65535));
    assertArrayEquals(new byte[]{-1, -1, -1, -1}, itole(-1));
    assertArrayEquals(new byte[]{1, 0, 0, 0}, itole(1));
    assertArrayEquals(new byte[]{-1, -1, -1, 127}, itole(2147483647));
  }

  @Test
  public void stoleTest() {
    short s = 255;
    assertArrayEquals(new byte[]{-1, 0}, stole(s));
    s = -1;
    assertArrayEquals(new byte[]{-1, -1}, stole(s));
    s = 1;
    assertArrayEquals(new byte[]{1, 0}, stole(s));
    s = 32767;
    assertArrayEquals(new byte[]{-1, 127}, stole(s));
  }

  @Test
  public void ltobeTest() {
    assertArrayEquals(new byte[]{0, 0, 0, 0, 0, 0, 0, -1}, ltobe(255l));
    assertArrayEquals(new byte[]{0, 0, 0, 0, 0, 0, -1, -1}, ltobe(65535l));
    assertArrayEquals(new byte[]{0, 0, 0, 0, -1, -1, -1, -1}, ltobe(4294967295l));
    assertArrayEquals(new byte[]{-1, -1, -1, -1, -1, -1, -1, -1}, ltobe(-1l));
    assertArrayEquals(new byte[]{0, 0, 0, 0, 0, 0, 0, 1}, ltobe(1l));
    assertArrayEquals(new byte[]{127, -1, -1, -1, -1, -1, -1, -1}, ltobe(9223372036854775807l));
  }

  @Test
  public void itobeTest() {
    assertArrayEquals(new byte[]{0, 0, 0, -1}, itobe(255));
    assertArrayEquals(new byte[]{0, 0, -1, -1}, itobe(65535));
    assertArrayEquals(new byte[]{-1, -1, -1, -1}, itobe(-1));
    assertArrayEquals(new byte[]{0, 0, 0, 1}, itobe(1));
    assertArrayEquals(new byte[]{127, -1, -1, -1}, itobe(2147483647));
  }

  @Test
  public void stobeTest() {
    short s = 255;
    assertArrayEquals(new byte[]{0, -1}, stobe(s));
    s = -1;
    assertArrayEquals(new byte[]{-1, -1}, stobe(s));
    s = 1;
    assertArrayEquals(new byte[]{0, 1}, stobe(s));
    s = 32767;
    assertArrayEquals(new byte[]{127, -1}, stobe(s));
  }

  @Test
  public void tob64Test() {

    // Test regular base64 encoding
    final String str = "\"Computer, compute to the last digit the value of pi\" -- Spock";
    String[] b64s = {
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ=="
    };
    for (int i = 0; i < b64s.length; i++) {
      assertEquals(b64s[i], tob64str(str.substring(0, str.length() - i).getBytes()));
      assertArrayEquals(b64s[i].getBytes(), tob64(str.substring(0, str.length() - i).getBytes()));
    }

    // Test URL modified base64 encoding
    b64s = new String[]{
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ"
    };
    for (int i = 0; i < b64s.length; i++) {
      assertEquals(b64s[i], tob64str(str.substring(0, str.length() - i).getBytes(),
          BitTwiddler.URL_MODIFIED_BASE64_CHARS));
      assertArrayEquals(b64s[i].getBytes(), tob64(str.substring(0, str.length() - i).getBytes(),
          BitTwiddler.URL_MODIFIED_BASE64_CHARS));
    }

    // Test YUI modified base64 encoding
    b64s = new String[]{
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ--"
    };
    for (int i = 0; i < b64s.length; i++) {
      assertEquals(b64s[i], tob64str(str.substring(0, str.length() - i).getBytes(),
          BitTwiddler.YUI_BASE64_CHARS));
      assertArrayEquals(b64s[i].getBytes(), tob64(str.substring(0, str.length() - i).getBytes(),
          BitTwiddler.YUI_BASE64_CHARS));
    }
  }

  @Test
  public void tohexTest() {
    assertArrayEquals(new byte[]{'1', '2', 'a', 'b', '3', '4', 'c', 'd', '5', '6', 'e', 'f'},
        tohex(new byte[]{0x12, (byte) 0xab, 0x34, (byte) 0xcd, 0x56, (byte) 0xef}, 0, 6, true));
    assertArrayEquals(new byte[]{'0', 'D', '0', 'E', '0', 'F', '1', 'A', '2', 'B'},
        tohex(new byte[]{0x0a, 0xb, 0xc, 0xd, 0xe, 0xf, 0x1a, 0x2b, 0x3c, 0x4d}, 3, 8, false));

    assertArrayEquals(new byte[]{'0', 'D', '0', 'E', '0', 'F', '1', 'A', '2', 'B'},
        tohex(new byte[]{0x0a, 0xb, 0xc, 0xd, 0xe, 0xf, 0x1a, 0x2b, 0x3c, 0x4d}, 3, 8));

    assertArrayEquals(
        new byte[]{'0', 'A', '0', 'B', '0', 'C', '0', 'D', '0', 'E', '0', 'F', '1', 'A', '2', 'B'},
        tohex(new byte[]{0x0a, 0xb, 0xc, 0xd, 0xe, 0xf, 0x1a, 0x2b}, false));
    assertArrayEquals(
        new byte[]{'0', 'a', '0', 'b', '0', 'c', '0', 'd', '0', 'e', '0', 'f', '1', 'a', '2', 'b'},
        tohex(new byte[]{0x0a, 0xb, 0xc, 0xd, 0xe, 0xf, 0x1a, 0x2b}, true));

    assertArrayEquals(
        new byte[]{'0', 'A', '0', 'B', '0', 'C', '0', 'D', '0', 'E', '0', 'F', '1', 'A', '2', 'B'},
        tohex(new byte[]{0x0a, 0xb, 0xc, 0xd, 0xe, 0xf, 0x1a, 0x2b}));

    // long
    assertArrayEquals(
        new byte[]{'0', 'A', '0', 'B', '0', 'C', '0', 'D', '0', 'E', '0', 'F', '1', 'A', '2', 'B'},
        tohex(0xa0b0c0d0e0f1a2bl, false));
    assertArrayEquals(
        new byte[]{'0', 'a', '0', 'b', '0', 'c', '0', 'd', '0', 'e', '0', 'f', '1', 'a', '2', 'b'},
        tohex(0xa0b0c0d0e0f1a2bl, true));

    assertArrayEquals(
        new byte[]{'0', 'A', '0', 'B', '0', 'C', '0', 'D', '0', 'E', '0', 'F', '1', 'A', '2', 'B'},
        tohex(0xa0b0c0d0e0f1a2bl));

    // int
    assertArrayEquals(new byte[]{'0', 'B', 'C', 'D', 'E', 'F', '0', '1'}, tohex(0xbcdef01, false));
    assertArrayEquals(new byte[]{'0', 'b', 'c', 'd', 'e', 'f', '0', '1'}, tohex(0xbcdef01, true));

    assertArrayEquals(new byte[]{'0', 'B', 'C', 'D', 'E', 'F', '0', '1'}, tohex(0xbcdef01));

    // short
    short s = 0xbcd;
    assertArrayEquals(new byte[]{'0', 'B', 'C', 'D'}, tohex(s, false));
    assertArrayEquals(new byte[]{'0', 'b', 'c', 'd'}, tohex(s, true));

    assertArrayEquals(new byte[]{'0', 'B', 'C', 'D'}, tohex(s));

    // byte
    byte b = 0x2a;
    assertArrayEquals(new byte[]{'2', 'A'}, tohex(b, false));
    assertArrayEquals(new byte[]{'2', 'a'}, tohex(b, true));

    assertArrayEquals(new byte[]{'2', 'A'}, tohex(b));
  }

  @Test
  public void tohexstrTest() {
    assertEquals("0000010203040a1b",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, 0, 8, true, true));
    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, 0, 8, false, true));
    assertEquals("10203040a1b",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, 0, 8, true, false));
    assertEquals("10203040a1b",
        tohexstr(new byte[]{0xf, 0xf, 0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b, 0xf, 0xf}, 2,
            10, true, false));
    assertEquals("0", tohexstr(new byte[]{0, 0, 0}, true, false));

    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, 0, 8, false));
    assertEquals("0000010203040a1b",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, 0, 8, true));
    assertEquals("0000010203040a1b",
        tohexstr(new byte[]{0xf, 0xf, 0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b, 0xf, 0xf}, 2,
            10, true));

    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, 0, 8));
    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0xf, 0xf, 0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b, 0xf, 0xf}, 2,
            10));

    assertEquals("0000010203040a1b",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, true, true));
    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, false, true));
    assertEquals("10203040a1b",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, true, false));

    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, false));
    assertEquals("0000010203040a1b",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}, true));

    assertEquals("0000010203040A1B",
        tohexstr(new byte[]{0x0, 0x0, 0x01, 0x02, 0x03, 0x04, 0x0a, 0x1b}));

    // long
    assertEquals("0a0b0c0d0e0f1a2b", tohexstr(0xa0b0c0d0e0f1a2bl, true, true));
    assertEquals("0A0B0C0D0E0F1A2B", tohexstr(0xa0b0c0d0e0f1a2bl, false, true));
    assertEquals("a0b0c0d0e0f1a2b", tohexstr(0xa0b0c0d0e0f1a2bl, true, false));

    assertEquals("0A0B0C0D0E0F1A2B", tohexstr(0xA0B0C0D0E0F1A2Bl, false));
    assertEquals("0a0b0c0d0e0f1a2b", tohexstr(0xa0b0c0d0e0f1a2bl, true));

    assertEquals("0A0B0C0D0E0F1A2B", tohexstr(0xa0b0c0d0e0f1a2bl));

    // int
    assertEquals("0a030f0d", tohexstr(0xa030f0d, true, true));
    assertEquals("0A030F0D", tohexstr(0xa030f0d, false, true));
    assertEquals("a030f0d", tohexstr(0xa030f0d, true, false));

    assertEquals("0A030F0D", tohexstr(0xa030f0d, false));
    assertEquals("0a030f0d", tohexstr(0xa030f0d, true));

    assertEquals("0A030F0D", tohexstr(0xa030f0d));

    // short
    short s = 0xade;
    assertEquals("0ade", tohexstr(s, true, true));
    assertEquals("0ADE", tohexstr(s, false, true));
    assertEquals("ade", tohexstr(s, true, false));

    assertEquals("0ADE", tohexstr(s, false));
    assertEquals("0ade", tohexstr(s, true));

    assertEquals("0ADE", tohexstr(s));

    // byte
    byte b = 0xa;
    assertEquals("0a", tohexstr(b, true, true));
    assertEquals("0A", tohexstr(b, false, true));
    assertEquals("a", tohexstr(b, true, false));

    assertEquals("0A", tohexstr(b, false));
    assertEquals("0a", tohexstr(b, true));

    assertEquals("0A", tohexstr(b));
  }

  @Test
  public void fromhexTest() {
    byte[] buf1 = new byte[]{-128, 127, -23, 45, 100};
    assertArrayEquals(buf1, fromhex(tohex(buf1)));
    assertArrayEquals(buf1, fromhex(tohex(buf1, true)));

    String tmp = "We're in the pipe, five by five";
    assertEquals(tmp, new String(fromhex(tohex(tmp.getBytes(UTF8))), UTF8));
    assertEquals(tmp, new String(fromhex(tohex(tmp.getBytes(UTF8), true)), UTF8));

    long ltmp = rand.nextLong();
    assertEquals(ltmp, lfromhex(tohex(ltmp)));
    assertEquals(ltmp, lfromhex(tohex(ltmp, true)));
    assertEquals(ltmp, lfromhex(tohexstr(ltmp)));
    assertEquals(ltmp, lfromhex(tohexstr(ltmp, true)));

    int itmp = rand.nextInt();
    assertEquals(itmp, ifromhex(tohex(itmp)));
    assertEquals(itmp, ifromhex(tohex(itmp, true)));
    assertEquals(itmp, ifromhex(tohexstr(itmp)));
    assertEquals(itmp, ifromhex(tohexstr(itmp, true)));

    short stmp = (short) rand.nextInt();
    assertEquals(stmp, sfromhex(tohex(stmp)));
    assertEquals(stmp, sfromhex(tohex(stmp, true)));
    assertEquals(stmp, sfromhex(tohexstr(stmp)));
    assertEquals(stmp, sfromhex(tohexstr(stmp, true)));

    byte[] btmp = new byte[1];
    rand.nextBytes(btmp);
    assertEquals(btmp[0], bfromhex(tohex(btmp[0])));
    assertEquals(btmp[0], bfromhex(tohex(btmp[0], true)));
    assertEquals(btmp[0], bfromhex(tohexstr(btmp[0])));
    assertEquals(btmp[0], bfromhex(tohexstr(btmp[0], true)));
  }

  @Test
  public void tobinTest() {
    assertArrayEquals(
        new byte[]{'1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(new byte[]{-1, 127,}, 0, 2));
    assertArrayEquals(
        new byte[]{'1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(new byte[]{0, -1, 127, 1}, 1, 3));

    assertArrayEquals(
        new byte[]{'1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(new byte[]{-1, 127,}));

    // long
    assertArrayEquals(new byte[]{
            '1', '1', '1', '1', '1', '1', '1', '1',
            '0', '1', '1', '1', '1', '1', '1', '1',
            '1', '1', '1', '1', '1', '1', '1', '1',
            '0', '1', '1', '1', '1', '1', '1', '1',
            '1', '1', '1', '1', '1', '1', '1', '1',
            '0', '1', '1', '1', '1', '1', '1', '1',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(0xff7fff7fff7f007fl));

    // long
    assertArrayEquals(new byte[]{
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(127l));

    // int
    assertArrayEquals(new byte[]{
            '1', '1', '1', '1', '1', '1', '1', '1',
            '0', '1', '1', '1', '1', '1', '1', '1',
            '0', '1', '1', '1', '1', '1', '1', '1',
            '0', '0', '0', '0', '0', '0', '0', '0'},
        tobin(0xff7f7f00));

    // short
    short s = (short) 0xff7f;
    assertArrayEquals(new byte[]{
            '1', '1', '1', '1', '1', '1', '1', '1',
            '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(s));
    s = 127;
    assertArrayEquals(new byte[]{
            '0', '0', '0', '0', '0', '0', '0', '0',
            '0', '1', '1', '1', '1', '1', '1', '1'},
        tobin(s));

    // byte
    byte b = (byte) 0x7f;
    assertArrayEquals(new byte[]{'0', '1', '1', '1', '1', '1', '1', '1'}, tobin(b));
  }

  @Test
  public void tobinstrTest() {
    assertEquals("0111111111111111", tobinstr(new byte[]{127, -1,}, 0, 2, true));
    assertEquals("111111111111111", tobinstr(new byte[]{127, -1,}, 0, 2, false));
    assertEquals("111111111111111", tobinstr(new byte[]{-1, 127, -1, 127}, 1, 3, false));

    assertEquals("111111111111111", tobinstr(new byte[]{-1, 127, -1, 127}, 1, 3));

    assertEquals("0111111111111111", tobinstr(new byte[]{127, -1,}, true));
    assertEquals("111111111111111", tobinstr(new byte[]{127, -1,}, false));

    assertEquals("111111111111111", tobinstr(new byte[]{127, -1,}));

    // long
    assertEquals("0000000000000000000000000000000000000000000000000000000001111111",
        tobinstr(127l, true));
    assertEquals("1111111", tobinstr(127l, false));

    // int
    assertEquals("00000000000000000000000001111111", tobinstr(127, true));
    assertEquals("1111111", tobinstr(127, false));

    // short
    short s = 127;
    assertEquals("0000000001111111", tobinstr(s, true));
    assertEquals("1111111", tobinstr(s, false));

    // byte
    byte b = 127;
    assertEquals("01111111", tobinstr(b, true));
    assertEquals("1111111", tobinstr(b, false));
  }

  @Test
  public void indexOfTest() throws IOException {
    byte[] p = {0x50, 0x4b, 0x03, 0x04}; // Reversed zip locale
    InputStream in = getClass().getResourceAsStream("1253.zip");
    final byte[] b = new byte[262144]; // 256KB
    in.read(b);
    in.close();
    assertEquals(0, indexOf(b, 0, b.length, p));
    assertEquals(0x520, indexOf(b, 10, b.length - 10, p));
    assertEquals(0, indexOf(b, p));
  }

  @Test
  public void searchTest() throws IOException {
    byte[] p = {0x50, 0x4b, 0x03, 0x04}; // Reversed zip locale
    long[] o = {0, 0x520, 0xbd6e, 0xBDAD, 0x15d9e, 0x1a2e5, 0x2bbb3, 0xdf2c7, 0xe52dc, 0x13829a};
    byte[] b = new byte[262144]; // 256KB

    InputStream in = getClass().getResourceAsStream("1253.zip");
    in.read(b);
    List<Integer> ir = search(b, 0, b.length, p);
    assertEquals(7, ir.size());
    for (int i = 0; i < ir.size(); i++) {
      assertEquals(o[i], (long) ir.get(i));
    }
    in.close();

    in = getClass().getResourceAsStream("1253.zip");
    in.read(b);
    ir = search(b, 1, 0x2bbb3 + 4, p);
    assertEquals(5, ir.size());
    for (int i = 1; i < ir.size(); i++) {
      assertEquals(o[i], (long) ir.get(i - 1));
    }
    in.close();

    in = getClass().getResourceAsStream("1253.zip");
    in.read(b);
    ir = search(b, p);
    assertEquals(7, ir.size());
    for (int i = 0; i < ir.size(); i++) {
      assertEquals(o[i], (long) ir.get(i));
    }
    in.close();

    in = getClass().getResourceAsStream("1253.zip");
    List<Long> lr = search(in, p);
    assertEquals(o.length, lr.size());
    for (int i = 0; i < lr.size(); i++) {
      assertEquals(o[i], (long) lr.get(i));
    }
    in.close();

    in = getClass().getResourceAsStream("1253.zip");
    lr = search(in, 0x2bbb3 + 4, p);
    assertEquals(7, lr.size());
    for (int i = 0; i < lr.size(); i++) {
      assertEquals(o[i], (long) lr.get(i));
    }
    in.close();

    in = getClass().getResourceAsStream("1253.zip");
    lr = search(in, 0x2bbb3 + 3, p);
    assertEquals(6, lr.size());
    for (int i = 0; i < lr.size(); i++) {
      assertEquals(o[i], (long) lr.get(i));
    }
    in.close();

    b = new byte[10240];
    in = getClass().getResourceAsStream("1253.zip");
    lr = search(in, p, b);
    assertEquals(o.length, lr.size());
    for (int i = 0; i < lr.size(); i++) {
      assertEquals(o[i], (long) lr.get(i));
    }
    in.close();

    in = getClass().getResourceAsStream("1253.zip");
    lr = search(in, 0x2bbb3 + 4, p, b);
    assertEquals(7, lr.size());
    for (int i = 0; i < lr.size(); i++) {
      assertEquals(o[i], (long) lr.get(i));
    }
    in.close();

    // Test search with an end boundary that will be less than the pattern length
    for (int i = 1253; i <= 1028; i++) {
      b = new byte[1024];
      in = getClass().getResourceAsStream("1253.zip");
      lr = search(in, i, p, b);
      assertEquals(1, lr.size());
      in.close();
    }

    in = getClass().getResourceAsStream("1028a.zip");
    lr = search(in, p, b);
    assertEquals(2, lr.size());
    in.close();

    in = getClass().getResourceAsStream("1028b.zip");
    lr = search(in, p, b);
    assertEquals(2, lr.size());
    in.close();

    in = getClass().getResourceAsStream("3.zip");
    lr = search(in, p, b);
    assertEquals(0, lr.size());
    in.close();
  }

  @Test
  public void fromb64test() {

    //base 64 test
    String b64s[] = {
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ=="
    };
    String strs[] = {
        "\"Computer, compute to the last digit the value of pi\" -- Spock",
        "\"Computer, compute to the last digit the value of pi\" -- Spoc",
        "\"Computer, compute to the last digit the value of pi\" -- Spo",
        "\"Computer, compute to the last digit the value of pi\" -- Sp",
        "\"Computer, compute to the last digit the value of pi\" -- S",
        "\"Computer, compute to the last digit the value of pi\" -- ",
        "\"Computer, compute to the last digit the value of pi\" --",
        "\"Computer, compute to the last digit the value of pi\" -",
        "\"Computer, compute to the last digit the value of pi\" ",
        "\"Computer, compute to the last digit the value of pi\"",
        "\"Computer, compute to the last digit the value of pi"
    };

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j],
            new String(fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, BASE64_CHARS));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j],
            new String(fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, BASE64_CHARS));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j], new String(fromb64(b64s[i].getBytes(), 0, strs[0].length() - j)));
        assertArrayEquals(strs[j].getBytes(), fromb64(b64s[i].getBytes(), 0, strs[0].length() - j));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64(b64s[i].getBytes(), BASE64_CHARS)));
      assertArrayEquals(strs[i].getBytes(), fromb64(b64s[i].getBytes(), BASE64_CHARS));
    }

//		url modified base 64 string test
    b64s = new String[]{
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ"
    };

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j], new String(
            fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, URL_MODIFIED_BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, URL_MODIFIED_BASE64_CHARS));
      }
    }
    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64(b64s[i].getBytes(), URL_MODIFIED_BASE64_CHARS)));
      assertArrayEquals(strs[i].getBytes(), fromb64(b64s[i].getBytes(), URL_MODIFIED_BASE64_CHARS));
    }

//				YUV base 64 string test
    b64s = new String[]{
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ--"
    };

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j],
            new String(fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, YUI_BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64(b64s[i].getBytes(), 0, strs[0].length() - j, YUI_BASE64_CHARS));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64(b64s[i].getBytes(), YUI_BASE64_CHARS)));
      assertArrayEquals(strs[i].getBytes(), fromb64(b64s[i].getBytes(), YUI_BASE64_CHARS));
    }
  }

  @Test
  public void fromb64strTest() {
    //base 64 test
    String b64s[] = {
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ==",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI=",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ=="
    };
    String strs[] = {
        "\"Computer, compute to the last digit the value of pi\" -- Spock",
        "\"Computer, compute to the last digit the value of pi\" -- Spoc",
        "\"Computer, compute to the last digit the value of pi\" -- Spo",
        "\"Computer, compute to the last digit the value of pi\" -- Sp",
        "\"Computer, compute to the last digit the value of pi\" -- S",
        "\"Computer, compute to the last digit the value of pi\" -- ",
        "\"Computer, compute to the last digit the value of pi\" --",
        "\"Computer, compute to the last digit the value of pi\" -",
        "\"Computer, compute to the last digit the value of pi\" ",
        "\"Computer, compute to the last digit the value of pi\"",
        "\"Computer, compute to the last digit the value of pi"
    };

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j],
            new String(fromb64str(b64s[i], 0, strs[0].length() - j, BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64str(b64s[i], 0, strs[0].length() - j, BASE64_CHARS));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j], new String(fromb64str(b64s[i], 0, strs[0].length() - j)));
        assertArrayEquals(strs[j].getBytes(), fromb64str(b64s[i], 0, strs[0].length() - j));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64str(b64s[i])));
      assertArrayEquals(strs[i].getBytes(), fromb64str(b64s[i]));
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64str(b64s[i], BASE64_CHARS)));
      assertArrayEquals(strs[i].getBytes(), fromb64str(b64s[i], BASE64_CHARS));
    }

//		url modified base 64 string test
    b64s = new String[]{
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ"
    };

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j],
            new String(fromb64str(b64s[i], 0, strs[0].length() - j, URL_MODIFIED_BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64str(b64s[i], 0, strs[0].length() - j, URL_MODIFIED_BASE64_CHARS));
      }
    }
    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64str(b64s[i], URL_MODIFIED_BASE64_CHARS)));
      assertArrayEquals(strs[i].getBytes(), fromb64str(b64s[i], URL_MODIFIED_BASE64_CHARS));
    }

//				YUV base 64 string test
    b64s = new String[]{
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvY2s-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3BvYw--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3Bv",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gU3A-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0gUw--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0g",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLS0-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIgLQ--",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSIg",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaSI-",
        "IkNvbXB1dGVyLCBjb21wdXRlIHRvIHRoZSBsYXN0IGRpZ2l0IHRoZSB2YWx1ZSBvZiBwaQ--"
    };

    for (int i = 0; i < b64s.length - 1; i++) {
      for (int j = i; j < strs.length - 1; j++) {
        assertEquals(strs[j],
            new String(fromb64str(b64s[i], 0, strs[0].length() - j, YUI_BASE64_CHARS)));
        assertArrayEquals(strs[j].getBytes(),
            fromb64str(b64s[i], 0, strs[0].length() - j, YUI_BASE64_CHARS));
      }
    }

    for (int i = 0; i < b64s.length - 1; i++) {
      assertEquals(strs[i], new String(fromb64str(b64s[i], YUI_BASE64_CHARS)));
      assertArrayEquals(strs[i].getBytes(), fromb64str(b64s[i], YUI_BASE64_CHARS));
    }
  }

  @Test
  public void lfromb64test() {
    long[] ls = {
        255l, 65535l, 4294967295l, -1l, 1l, 9223372036854775807l
    };

    for (long l : ls) {
      byte[] b64 = tob64(ltobe(l));
      byte[] fromb64 = fromb64(b64);
      assertEquals(l, betol(fromb64));
      assertArrayEquals(ltobe(l), fromb64);

      b64 = tob64(ltobe(l), URL_MODIFIED_BASE64_CHARS);
      fromb64 = fromb64(b64, URL_MODIFIED_BASE64_CHARS);
      assertEquals(l, betol(fromb64));
      assertArrayEquals(ltobe(l), fromb64);

      b64 = tob64(ltobe(l), YUI_BASE64_CHARS);
      fromb64 = fromb64(b64, YUI_BASE64_CHARS);
      assertEquals(l, betol(fromb64));
      assertArrayEquals(ltobe(l), fromb64);
    }
  }

  @Test
  public void ifromb64test() {
    int[] is = {
        255, 65535, -1, 1, 2147483647
    };

    for (int i : is) {
      byte[] b64 = tob64(itobe(i));
      byte[] fromb64 = fromb64(b64);
      assertEquals(i, betoi(fromb64));
      assertArrayEquals(itobe(i), fromb64);

      b64 = tob64(itobe(i), URL_MODIFIED_BASE64_CHARS);
      fromb64 = fromb64(b64, URL_MODIFIED_BASE64_CHARS);
      assertEquals(i, betoi(fromb64));
      assertArrayEquals(itobe(i), fromb64);

      b64 = tob64(itobe(i), YUI_BASE64_CHARS);
      fromb64 = fromb64(b64, YUI_BASE64_CHARS);
      assertEquals(i, betoi(fromb64));
      assertArrayEquals(itobe(i), fromb64);
    }
  }

  @Test
  public void sfromb64test() {
    short[] ss = {
        255, -1, 1, 32767
    };

    for (short s : ss) {
      byte[] b64 = tob64(stobe(s));
      byte[] fromb64 = fromb64(b64);
      assertEquals(s, betos(fromb64));
      assertArrayEquals(stobe(s), fromb64);

      b64 = tob64(stobe(s), URL_MODIFIED_BASE64_CHARS);
      fromb64 = fromb64(b64, URL_MODIFIED_BASE64_CHARS);
      assertEquals(s, betos(fromb64));
      assertArrayEquals(stobe(s), fromb64);

      b64 = tob64(stobe(s), YUI_BASE64_CHARS);
      fromb64 = fromb64(b64, YUI_BASE64_CHARS);
      assertEquals(s, betos(fromb64));
      assertArrayEquals(stobe(s), fromb64);
    }
  }

  @Test
  public void bytefromb64test() {
    byte[] bs = {
        1, -1, 111, 127, -128, 0, -111
    };
    for (byte b : bs) {
      byte[] b64 = tob64(b);
      byte fromb64 = bfromb64(b64);
      assertEquals(b, fromb64);

      b64 = tob64(b, URL_MODIFIED_BASE64_CHARS);
      fromb64 = bfromb64(b64, URL_MODIFIED_BASE64_CHARS);
      assertEquals(b, fromb64);

      b64 = tob64(b, YUI_BASE64_CHARS);
      fromb64 = bfromb64(b64, YUI_BASE64_CHARS);
      assertEquals(b, fromb64);
    }
  }

  @Test
  public void bfromb64strTest() {
    byte[] bs = {
        1, -1, 111, 127, -128, 0, -111
    };
    for (byte b : bs) {
      byte[] b64 = tob64(b);
      byte fromb64 = bfromb64str(new String(b64));
      assertEquals(b, fromb64);

      b64 = tob64(b, URL_MODIFIED_BASE64_CHARS);
      fromb64 = bfromb64str(new String(b64), URL_MODIFIED_BASE64_CHARS);
      assertEquals(b, fromb64);

      b64 = tob64(b, YUI_BASE64_CHARS);
      fromb64 = bfromb64str(new String(b64), YUI_BASE64_CHARS);
      assertEquals(b, fromb64);
    }
  }

  @Test
  public void sfromb64strTest() {
    short[] ss = {
        255, -1, 1, 32767
    };

    for (short s : ss) {
      byte[] b64 = tob64(stobe(s));
      byte[] fromb64 = fromb64str(new String(b64));
      assertEquals(s, betos(fromb64));
      assertArrayEquals(stobe(s), fromb64);

      b64 = tob64(stobe(s), URL_MODIFIED_BASE64_CHARS);
      fromb64 = fromb64str(new String(b64), URL_MODIFIED_BASE64_CHARS);
      assertEquals(s, betos(fromb64));
      assertArrayEquals(stobe(s), fromb64);

      b64 = tob64(stobe(s), YUI_BASE64_CHARS);
      fromb64 = fromb64str(new String(b64), YUI_BASE64_CHARS);
      assertEquals(s, betos(fromb64));
      assertArrayEquals(stobe(s), fromb64);
    }
  }

  @Test
  public void lfromb64strTest() {
    long[] ls = {
        255l, 65535l, 4294967295l, -1l, 1l, 9223372036854775807l
    };

    for (long l : ls) {
      byte[] b64 = tob64(ltobe(l));
      byte[] fromb64 = fromb64str(new String(b64));
      assertEquals(l, betol(fromb64));
      assertArrayEquals(ltobe(l), fromb64);

      b64 = tob64(ltobe(l), URL_MODIFIED_BASE64_CHARS);
      fromb64 = fromb64str(new String(b64), URL_MODIFIED_BASE64_CHARS);
      assertEquals(l, betol(fromb64));
      assertArrayEquals(ltobe(l), fromb64);

      b64 = tob64(ltobe(l), YUI_BASE64_CHARS);
      fromb64 = fromb64str(new String(b64), YUI_BASE64_CHARS);
      assertEquals(l, betol(fromb64));
      assertArrayEquals(ltobe(l), fromb64);
    }
  }

  @Test
  public void ifromb64strTest() {
    int[] is = {
        255, 65535, -1, 1, 2147483647
    };

    for (int i : is) {
      byte[] b64 = tob64(itobe(i));
      byte[] fromb64 = fromb64str(new String(b64));
      assertEquals(i, betoi(fromb64));
      assertArrayEquals(itobe(i), fromb64);

      b64 = tob64(itobe(i), URL_MODIFIED_BASE64_CHARS);
      fromb64 = fromb64str(new String(b64), URL_MODIFIED_BASE64_CHARS);
      assertEquals(i, betoi(fromb64));
      assertArrayEquals(itobe(i), fromb64);

      b64 = tob64(itobe(i), YUI_BASE64_CHARS);
      fromb64 = fromb64str(new String(b64), YUI_BASE64_CHARS);
      assertEquals(i, betoi(fromb64));
      assertArrayEquals(itobe(i), fromb64);
    }
  }
}
