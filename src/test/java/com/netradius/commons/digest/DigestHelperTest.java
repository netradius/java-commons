/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.digest;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for DigestHelper class.
 */
public class DigestHelperTest {

  @Test
  public void testGetAlgorithms() {
    DigestHelper.getAlgorithms().forEach(System.out::println);
  }

  @Test
  public void testDigest() throws IOException {
    File file = new File("src/test/resources/com/netradius/commons/digest/file1.txt");
    String sha1 = DigestHelper.sha1(file);
    System.out.println(String.format("%s %s", sha1, file.getPath()));
    assertEquals("703b38205248ec806f18967940c00eb86ed950cf", sha1);

    String sha256 = DigestHelper.sha256(file);
    System.out.println(String.format("%s %s", sha256, file.getPath()));
    assertEquals("3c74aa627a3e5ee55154dde525dd434f48f5c78efb86485da0b9ae2f6a3971ff", sha256);

    String sha512 = DigestHelper.sha512(file);
    System.out.println(String.format("%s %s", sha512, file.getPath()));
    assertEquals("634e9e5e3580334b4ff16c5bf2f9293a6818210a71171648a93b38f42f6aa1e04b3463" +
        "7b357c4e7cab386af7795e107a610b3dd8d40f3b83add863cf4926adbc", sha512);
  }

}
