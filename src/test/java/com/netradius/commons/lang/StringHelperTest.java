/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for StringHelper.
 *
 * @author Erik R. Jensen
 */
public class StringHelperTest {

  @Test
  public void testCamelCaseToUnderscore() {
    assertEquals("camel_case", StringHelper.camelCaseToUnderscore("camelCase"));
    assertEquals("c_a_m_e_l", StringHelper.camelCaseToUnderscore("CAMEL"));
  }

  @Test
  public void testPascalCaseToUnderscore() {
    assertEquals("pascal_case", StringHelper.pascalCaseToUnderscore("PascalCase"));
    assertEquals("p_a_s_c_a_l", StringHelper.pascalCaseToUnderscore("PASCAL"));
  }

  @Test
  public void testUnderscoreToCamelCase() {
    assertEquals("camelCase", StringHelper.underscoreToCamelCase("camel_case"));
    assertEquals("cAMEL", StringHelper.underscoreToCamelCase("c_a_m_e_l"));
    assertEquals("camelCase", StringHelper.underscoreToCamelCase("camelCase"));
  }

  @Test
  public void testUnderscoreToPascalCase() {
    assertEquals("PascalCase", StringHelper.underscoreToPascalCase("pascal_case"));
    assertEquals("PASCAL", StringHelper.underscoreToPascalCase("p_a_s_c_a_l"));
    assertEquals("PascalCase", StringHelper.underscoreToPascalCase("PascalCase"));
  }

  @Test
  public void testTrimLeadingWhitespace() {
    assertEquals(null, StringHelper.trimLeadingWhitespace(null));
    assertEquals("", StringHelper.trimLeadingWhitespace(""));
    assertEquals("test", StringHelper.trimLeadingWhitespace("test"));
    assertEquals("te st ", StringHelper.trimLeadingWhitespace("te st "));
    assertEquals("te st ", StringHelper.trimLeadingWhitespace(" te st "));
    assertEquals("test", StringHelper.trimLeadingWhitespace("             test"));
    assertEquals("test", StringHelper.trimLeadingWhitespace("		test"));
  }

  @Test
  public void testTrimTrailingWhitespace() {
    assertEquals(null, StringHelper.trimTrailingWhitespace(null));
    assertEquals("", StringHelper.trimTrailingWhitespace(""));
    assertEquals("test", StringHelper.trimTrailingWhitespace("test"));
    assertEquals(" te st", StringHelper.trimTrailingWhitespace(" te st"));
    assertEquals(" te st", StringHelper.trimTrailingWhitespace(" te st "));
    assertEquals("test", StringHelper.trimTrailingWhitespace("test              "));
    assertEquals("test", StringHelper.trimTrailingWhitespace("test		"));
  }

  @Test
  public void testTrimWhitespace() {
    assertEquals(null, StringHelper.trimWhitespace(null));
    assertEquals("", StringHelper.trimWhitespace(""));
    assertEquals("test", StringHelper.trimWhitespace("test"));
    assertEquals("te st", StringHelper.trimWhitespace(" te st"));
    assertEquals("te st", StringHelper.trimWhitespace("te st "));
    assertEquals("te st", StringHelper.trimWhitespace(" te st "));
    assertEquals("test", StringHelper.trimWhitespace("        test              "));
    assertEquals("test", StringHelper.trimWhitespace("		test		"));
  }

  @Test
  public void testIsEmpty() {
    assertTrue(StringHelper.isEmpty(null));
    assertTrue(StringHelper.isEmpty(""));
    assertFalse(StringHelper.isEmpty(" "));
    assertFalse(StringHelper.isEmpty("      "));
    assertFalse(StringHelper.isEmpty("		"));
    assertFalse(StringHelper.isEmpty("		     "));
    assertFalse(StringHelper.isEmpty("test"));
    assertFalse(StringHelper.isEmpty("  test  "));
  }

  @Test
  public void testIsNotEmpty() {
    assertFalse(StringHelper.isNotEmpty(null));
    assertFalse(StringHelper.isNotEmpty(""));
    assertTrue(StringHelper.isNotEmpty(" "));
    assertTrue(StringHelper.isNotEmpty("      "));
    assertTrue(StringHelper.isNotEmpty("		"));
    assertTrue(StringHelper.isNotEmpty("		     "));
    assertTrue(StringHelper.isNotEmpty("test"));
    assertTrue(StringHelper.isNotEmpty("  test  "));
  }

  @Test
  public void testIsBlank() {
    assertTrue(StringHelper.isBlank(null));
    assertTrue(StringHelper.isBlank(""));
    assertTrue(StringHelper.isBlank(" "));
    assertTrue(StringHelper.isBlank("      "));
    assertTrue(StringHelper.isBlank("		"));
    assertTrue(StringHelper.isBlank("		     "));
    assertFalse(StringHelper.isBlank("test"));
    assertFalse(StringHelper.isBlank("  test  "));
  }

  @Test
  public void testIsNotBlank() {
    assertFalse(StringHelper.isNotBlank(null));
    assertFalse(StringHelper.isNotBlank(""));
    assertFalse(StringHelper.isNotBlank(" "));
    assertFalse(StringHelper.isNotBlank("      "));
    assertFalse(StringHelper.isNotBlank("		"));
    assertFalse(StringHelper.isNotBlank("		     "));
    assertTrue(StringHelper.isNotBlank("test"));
    assertTrue(StringHelper.isNotBlank("  test  "));
  }
}
