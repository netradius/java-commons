/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Pattern;

import static com.netradius.commons.lang.ValidationHelper.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit tests for ValidationHelper.
 *
 * @author Erik R. Jensen
 */
public class ValidationHelperTest {

  @Test
  public void testNotNull1() {
    assertThrows(IllegalArgumentException.class, () -> notNull(null, "array"));
  }

  @Test
  public void testNotNull2() {
    notNull("test", "array");
  }

  @Test
  public void testNotEmpty1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((String) null, "array"));
  }

  @Test
  public void testNotEmpty2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty("", "array"));
  }

  @Test
  public void testNotEmpty3() {
    notEmpty("  ", "array");
  }

  @Test
  public void testNotEmpty4() {
    notEmpty("test", "array");
  }

  @Test
  public void testNotBlank1() {
    assertThrows(IllegalArgumentException.class, () -> notBlank((String) null, "array"));
  }

  @Test
  public void testNotBlank2() {
    assertThrows(IllegalArgumentException.class, () -> notBlank("", "array"));
  }

  @Test
  public void testNotBlank3() {
    assertThrows(IllegalArgumentException.class, () -> notBlank("  ", "array"));
  }

  @Test
  public void testNotBlank4() {
    notBlank("test", "array");
  }

  @Test
  public void testForEmptyIntA1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((int[]) null, "array"));
  }

  @Test
  public void testForEmptyIntA2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(new int[0], "array"));
  }

  @Test
  public void testForEmptyIntA3() {
    notEmpty(new int[]{1}, "array");
  }

  @Test
  public void testForEmptyShortA1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((short[]) null, "array"));
  }

  @Test
  public void testForEmptyShortA2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(new short[0], "array"));
  }

  @Test
  public void testForEmptyShortA3() {
    notEmpty(new short[]{1}, "array");
  }

  @Test
  public void testForEmptyLongA1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((long[]) null, "array"));
  }

  @Test
  public void testForEmptyLongA2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(new long[0], "array"));
  }

  @Test
  public void testForEmptyLong3() {
    notEmpty(new long[]{1}, "array");
  }

  @Test
  public void testForEmptyFloatA1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((float[]) null, "array"));
  }

  @Test
  public void testForEmptyFloatA2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(new float[0], "array"));
  }

  @Test
  public void testForEmptyFloatA3() {
    notEmpty(new float[]{1f}, "array");
  }

  @Test
  public void testForEmptyDoubleA1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((double[]) null, "array"));
  }

  @Test
  public void testForEmptyDoubleA2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(new double[0], "array"));
  }

  @Test
  public void testForEmptyDoubleA3() {
    notEmpty(new double[]{1}, "array");
  }

  @Test
  public void testForEmptyObjectA1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty((Object[]) null, "array"));
  }

  @Test
  public void testForEmptyObjectA2() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(new Object[0], "array"));
  }

  @Test
  public void testForEmptyObjectA3() {
    notEmpty(new Object[]{1}, "array");
  }

  @Test
  public void testForEmptyCollection1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(Collections.EMPTY_LIST, "list"));
  }

  @Test
  public void testForEmptyCollection2() {
    notEmpty(Arrays.asList("something", "somethingelse"), "list");
  }

  @Test
  public void testForEmptyMap1() {
    assertThrows(IllegalArgumentException.class, () -> notEmpty(Collections.EMPTY_MAP, "list"));
  }

  @Test
  public void testForEmptyMap2() {
    notEmpty(new HashMap<String, String>() {{put("somekey", "somevalue");}}, "map");
  }

  @Test
  public void minInt1() {
    int a = 0, b = 1;
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minInt2() {
    int a = 1, b = 1;
    min(a, b, "a");
  }

  @Test
  public void minShort1() {
    short a = 0, b = 1;
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minShort2() {
    short a = 1, b = 1;
    min(a, b, "a");
  }

  @Test
  public void minLong1() {
    long a = 0, b = 1;
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minLong2() {
    long a = 1, b = 1;
    min(a, b, "a");
  }

  @Test
  public void minFloat1() {
    float a = 0, b = 1;
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minFloat2() {
    float a = 1, b = 1;
    min(a, b, "a");
  }

  @Test
  public void minDouble1() {
    double a = 0, b = 1;
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minDouble2() {
    double a = 1, b = 1;
    min(a, b, "a");
  }

  @Test
  public void minBigInteger1() {
    BigInteger a = new BigInteger("0");
    BigInteger b = new BigInteger("1");
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minBigInteger2() {
    BigInteger a = new BigInteger("1");
    BigInteger b = new BigInteger("1");
    min(a, b, "a");
  }

  @Test
  public void minBigDecimal1() {
    BigDecimal a = new BigDecimal("0");
    BigDecimal b = new BigDecimal("1");
    assertThrows(IllegalArgumentException.class, () -> min(a, b, "a"));
  }

  @Test
  public void minBigDecimal2() {
    BigDecimal a = new BigDecimal("1");
    BigDecimal b = new BigDecimal("1");
    min(a, b, "a");
  }

  @Test
  public void maxInt1() {
    int a = 1, b = 0;
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxInt2() {
    int a = 1, b = 1;
    max(a, b, "a");
  }

  @Test
  public void maxShort1() {
    short a = 1, b = 0;
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxShort2() {
    short a = 1, b = 1;
    max(a, b, "a");
  }

  @Test
  public void maxLong1() {
    long a = 1, b = 0;
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxLong2() {
    long a = 1, b = 1;
    max(a, b, "a");
  }

  @Test
  public void maxFloat1() {
    float a = 1, b = 0;
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxFloat2() {
    float a = 1, b = 1;
    max(a, b, "a");
  }

  @Test
  public void maxDouble1() {
    double a = 1, b = 0;
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxDouble2() {
    double a = 1, b = 1;
    max(a, b, "a");
  }

  @Test
  public void maxBigInteger1() {
    BigInteger a = new BigInteger("1");
    BigInteger b = new BigInteger("0");
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxBitInteger2() {
    BigInteger a = new BigInteger("1");
    BigInteger b = new BigInteger("1");
    max(a, b, "a");
  }

  @Test
  public void maxBigDecimal1() {
    BigDecimal a = new BigDecimal("1");
    BigDecimal b = new BigDecimal("0");
    assertThrows(IllegalArgumentException.class, () -> max(a, b, "a"));
  }

  @Test
  public void maxBigDecimal2() {
    BigDecimal a = new BigDecimal("1");
    BigDecimal b = new BigDecimal("1");
    max(a, b, "a");
  }

  @Test
  public void betweenInt1() {
    int a = 0, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenInt2() {
    int a = 3, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenInt3() {
    int a = 1, b = 1, c = 2;
    between(a, b, c, "a");
  }

  @Test
  public void betweenShort1() {
    short a = 0, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenShort2() {
    short a = 3, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenShort3() {
    short a = 1, b = 1, c = 2;
    between(a, b, c, "a");
  }

  @Test
  public void betweenLong1() {
    long a = 0, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenLong2() {
    long a = 3, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenLong3() {
    long a = 1, b = 1, c = 2;
    between(a, b, c, "a");
  }

  @Test
  public void betweenFloat1() {
    float a = 0, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenFloat2() {
    float a = 3, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenFloat3() {
    float a = 1, b = 1, c = 2;
    between(a, b, c, "a");
  }

  @Test
  public void betweenDouble1() {
    double a = 0, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenDouble2() {
    double a = 3, b = 1, c = 2;
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenDouble3() {
    double a = 1, b = 1, c = 2;
    between(a, b, c, "a");
  }

  @Test
  public void betweenBigInteger1() {
    BigInteger a = new BigInteger("0");
    BigInteger b = new BigInteger("1");
    BigInteger c = new BigInteger("2");
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenBigInteger2() {
    BigInteger a = new BigInteger("1");
    BigInteger b = new BigInteger("1");
    BigInteger c = new BigInteger("2");
    between(a, b, c, "a");
  }

  @Test
  public void betweenBigDecimal1() {
    BigDecimal a = new BigDecimal("0");
    BigDecimal b = new BigDecimal("1");
    BigDecimal c = new BigDecimal("2");
    assertThrows(IllegalArgumentException.class, () -> between(a, b, c, "a"));
  }

  @Test
  public void betweenBigDecimal2() {
    BigDecimal a = new BigDecimal("1");
    BigDecimal b = new BigDecimal("1");
    BigDecimal c = new BigDecimal("2");
    between(a, b, c, "a");
  }

  @Test
  public void regexTest1() {
    regex("thisisatest", "[a-z]+", "value");
  }

  @Test
  public void regextTest2() {
    assertThrows(IllegalArgumentException.class, () -> regex("this is a test", "[a-z]+", "value"));
  }

  @Test
  public void regexTest3() {
    regex("thisisatest", Pattern.compile("[a-z]+"), "value");
  }

  @Test
  public void regextTest4() {
    assertThrows(IllegalArgumentException.class, () -> regex("this is a test", Pattern.compile("[a-z]+"), "value"));
  }

}
