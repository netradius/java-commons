/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Unit tests for the AlphanumComparator class.
 *
 * @author Erik R. Jensen
 */
public class AlphanumComparatorTest {

  private static final String[] SET1 = {
      "x1.doc",
      "x2.doc",
      "x3.doc",
      "x4.doc",
      "x5.doc",
      "x6.doc",
      "x7.doc",
      "x8.doc",
      "x9.doc",
      "x10.doc",
      "x11.doc",
      "x12.doc",
      "x13.doc",
      "x14.doc",
      "x15.doc",
      "x16.doc",
      "x17.doc",
      "x18.doc",
      "x19.doc",
      "x20.doc",
      "x100.doc",
      "x101.doc",
      "x102.doc",
      "x1001.doc",
      "x1002.doc",
  };

  private static final String[] SET2 = {
      "10X Radonius",
      "20X Radonius",
      "20X Radonius Prime",
      "30X Radonius",
      "40X Radonius",
      "200X Radonius",
      "1000X Radonius Maximus",
      "Allegia 6R Clasteron",
      "Allegia 50 Clasteron",
      "Allegia 50B Clasteron",
      "Allegia 51 Clasteron",
      "Allegia 500 Clasteron",
      "Alpha 2",
      "Alpha 2A",
      "Alpha 2A-900",
      "Alpha 2A-8000",
      "Alpha 100",
      "Alpha 200",
      "Callisto Morphamax",
      "Callisto Morphamax 500",
      "Callisto Morphamax 600",
      "Callisto Morphamax 700",
      "Callisto Morphamax 5000",
      "Callisto Morphamax 6000 SE",
      "Callisto Morphamax 6000 SE2",
      "Callisto Morphamax 7000",
      "Xiph Xlater 5",
      "Xiph Xlater 40",
      "Xiph Xlater 50",
      "Xiph Xlater 58",
      "Xiph Xlater 300",
      "Xiph Xlater 500",
      "Xiph Xlater 2000",
      "Xiph Xlater 5000",
      "Xiph Xlater 10000"
  };

  private static final String[] SET3 = {
      "foo0004moo01",
      "foo004moo02",
      "foo4moo3"
  };

  @Test
  public void test() {
    AlphanumComparator c = new AlphanumComparator();

    String[] a = Arrays.copyOf(SET1, SET1.length);
    ArrayHelper.shuffle(a);
    assertFalse(ArrayHelper.equals(a, SET1)); // just a sanity check on shuffle
    Arrays.sort(a, c);
    for (int i = 0; i < a.length; i++) {
      assertEquals(a[i], SET1[i]);
    }

    a = Arrays.copyOf(SET2, SET2.length);
    ArrayHelper.shuffle(a);
    Arrays.sort(a, c);
    for (int i = 0; i < a.length; i++) {
      assertEquals(a[i], SET2[i]);
    }

    a = Arrays.copyOf(SET3, SET3.length);
    ArrayHelper.shuffle(a);
    Arrays.sort(a, c);
    for (int i = 0; i < a.length; i++) {
      assertEquals(a[i], SET3[i]);
    }
  }
}
