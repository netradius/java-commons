/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for ArrayHelper.
 *
 * @author Erik R. Jensen
 */
public class ArrayHelperTest {

  @Test
  public void testConcat() {
    assertArrayEquals(new byte[]{5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6, 6, 6},
        ArrayHelper.concat(
            new byte[]{5, 4, 3, 2, 1},
            new byte[]{1, 2, 3, 4, 5},
            new byte[]{6, 6, 6}));
    assertArrayEquals(new short[]{5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6, 6, 6},
        ArrayHelper.concat(
            new short[]{5, 4, 3, 2, 1},
            new short[]{1, 2, 3, 4, 5},
            new short[]{6, 6, 6}));
    assertArrayEquals(new int[]{5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6, 6, 6},
        ArrayHelper.concat(
            new int[]{5, 4, 3, 2, 1},
            new int[]{1, 2, 3, 4, 5},
            new int[]{6, 6, 6}));
    assertArrayEquals(new long[]{5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6, 6, 6},
        ArrayHelper.concat(
            new long[]{5, 4, 3, 2, 1},
            new long[]{1, 2, 3, 4, 5},
            new long[]{6, 6, 6}));
    // TODO JUnit does not support this
//		assertArrayEquals(new float[] {5.0f, 4.1f, 3.2f, 2.3f, 1.4f, 1.5f, 2.6f, 3.7f, 4.8f, 5.9f, 6.0f, 6.1f, 6.2f},
//				ArrayHelper.concat(
//						new float[] {5.0f, 4.1f, 3.2f, 2.3f, 1.4f},
//						new float[] {1.5f, 2.6f, 3.7f, 4.8f, 5.9f},
//						new float[] {6.0f, 6.1f, 6.2f}));
    // TODO JUnit does not support this
//		assertArrayEquals(new double[] {5.0, 4.1, 3.2, 2.3, 1.4, 1.5, 2.6, 3.7, 4.8, 5.9, 6.0, 6.1, 6.2},
//				ArrayHelper.concat(
//						new double[] {5.0, 4.1, 3.2, 2.3, 1.4},
//						new double[] {1.5, 2.6, 3.7, 4.8, 5.9},
//						new double[] {6.0, 6.1, 6.2}));
    assertArrayEquals(new String[]{"5", "4", "3", "2", "1", "1", "2", "3", "4", "5", "6", "6", "6"},
        ArrayHelper.concat(String.class,
            new String[]{"5", "4", "3", "2", "1"},
            new String[]{"1", "2", "3", "4", "5"},
            new String[]{"6", "6", "6"}));
  }

  @Test
  public void testReverse() {
    byte[] b = new byte[]{1, 2, 3, 4, 5};
    assertArrayEquals(new byte[]{5, 4, 3, 2, 1}, ArrayHelper.reverse(b));
    short[] s = new short[]{1, 2, 3, 4, 5};
    assertArrayEquals(new short[]{5, 4, 3, 2, 1}, ArrayHelper.reverse(s));
    int[] i = new int[]{1, 2, 3, 4, 5};
    assertArrayEquals(new int[]{5, 4, 3, 2, 1}, ArrayHelper.reverse(i));
    long[] l = new long[]{1, 2, 3, 4, 5};
    assertArrayEquals(new long[]{5, 4, 3, 2, 1}, ArrayHelper.reverse(l));
    float[] f = new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f};
//		assertArrayEquals(new float[] {5.5f, 4.4f, 3.3f, 2.2f, 1.1f}, ArrayHelper.reverse(f)); // TODO JUnit does not support this.
    double[] d = new double[]{1.1d, 2.2d, 3.3d, 4.4d, 5.5d};
//		assertArrayEquals(new double[] {5.5d, 4.4d, 3.3d, 2.2d, 1.1d}, ArrayHelper.reverse(d)); // TODO JUnit does not support this.
    char[] c = new char[]{'a', 'b', 'c', 'd', 'e'};
    assertArrayEquals(new char[]{'e', 'd', 'c', 'b', 'a'}, ArrayHelper.reverse(c));
    String[] str = new String[]{"1", "2", "3", "4", "5"};
    assertArrayEquals(new String[]{"5", "4", "3", "2", "1"}, ArrayHelper.reverse(str));
  }

  @Test
  public void testEquals() {
    assertTrue(ArrayHelper.equals(new byte[]{1, 2, 3, 4, 5}, new byte[]{1, 2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new byte[]{1, 2, 3, 4, 5}, new byte[]{2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new byte[]{1, 2, 3, 4, 5}, new byte[]{1, 2, 3, 4, 1}));

    assertTrue(ArrayHelper.equals(new short[]{1, 2, 3, 4, 5}, new short[]{1, 2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new short[]{1, 2, 3, 4, 5}, new short[]{2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new short[]{1, 2, 3, 4, 5}, new short[]{1, 2, 3, 4, 1}));

    assertTrue(ArrayHelper.equals(new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new int[]{1, 2, 3, 4, 5}, new int[]{2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 1}));

    assertTrue(ArrayHelper.equals(new long[]{1, 2, 3, 4, 5}, new long[]{1, 2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new long[]{1, 2, 3, 4, 5}, new long[]{2, 3, 4, 5}));
    assertFalse(ArrayHelper.equals(new long[]{1, 2, 3, 4, 5}, new long[]{1, 2, 3, 4, 1}));

    assertTrue(ArrayHelper.equals(new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f},
        new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f}));
    assertFalse(ArrayHelper
        .equals(new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f}, new float[]{1.1f, 2.2f, 3.3f, 4.4f}));
    assertFalse(ArrayHelper.equals(new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f},
        new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.1f}));

    assertTrue(ArrayHelper.equals(new double[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f},
        new double[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f}));
    assertFalse(ArrayHelper
        .equals(new double[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f}, new double[]{1.1f, 2.2f, 3.3f, 4.4f}));
    assertFalse(ArrayHelper.equals(new double[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f},
        new double[]{1.1f, 2.2f, 3.3f, 4.4f, 5.1f}));

    assertTrue(ArrayHelper
        .equals(new char[]{'a', 'b', 'c', 'd', 'e'}, new char[]{'a', 'b', 'c', 'd', 'e'}));
    assertFalse(
        ArrayHelper.equals(new char[]{'a', 'b', 'c', 'd', 'e'}, new char[]{'a', 'b', 'c', 'd'}));
    assertFalse(ArrayHelper
        .equals(new char[]{'a', 'b', 'c', 'd', 'e'}, new char[]{'a', 'b', 'c', 'd', 'z'}));
  }
}
