/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.lang;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for ReflectionHelper.
 *
 * @author Erik R. Jensen
 */
public class ReflectionHelperTest {

  @Test
  public void testNewInstance() {
    assertNotNull(ReflectionHelper.newInstance(NewInstance1.class));
    assertNotNull(ReflectionHelper.newInstance(NewInstance2.class, new TestClass()));
    assertNotNull(ReflectionHelper.newInstance(NewInstance2.class, new ExtendedTestClass()));
  }

  public static class NewInstance1 {

  }

  public static class NewInstance2 {

    public NewInstance2(TestClass o) {
    }
  }

  @Test
  public void testFindClass() {
    assertNotNull(
        ReflectionHelper.findClass("com.netradius.commons.lang.ReflectionHelperTest$NewInstance1"));
    assertNull(ReflectionHelper.findClass("com.netradius.commons.lang.CaptainKirk"));
  }

  @Test
  public void testFindField() {
    assertNotNull(ReflectionHelper.findField(FindField1.class, "text1"));
    assertNotNull(ReflectionHelper.findField(FindField1.class, "text2"));
    assertNull(ReflectionHelper.findField(FindField1.class, "text3"));
    assertNotNull(ReflectionHelper.findField(FindField2.class, "text1"));
    assertNotNull(ReflectionHelper.findField(FindField2.class, "text2"));
    assertNotNull(ReflectionHelper.findField(FindField2.class, "text3"));
  }

  public static class FindField1 {

    private static String text1;
    private String text2;
  }

  public static class FindField2 extends FindField1 {

    private String text3;
  }

  @Test
  public void testFindMethod() {
    assertNotNull(ReflectionHelper.findMethod(FindMethod1.class, "test1"));
    assertNotNull(ReflectionHelper.findMethod(FindMethod1.class, "test2"));
    assertNotNull(ReflectionHelper.findMethod(FindMethod1.class, "test2", String.class));
    assertNotNull(ReflectionHelper.findMethod(FindMethod1.class, "test3"));
    assertNotNull(ReflectionHelper.findMethod(FindMethod2.class, "test1"));
    assertNotNull(ReflectionHelper.findMethod(FindMethod2.class, "test2"));
    assertNotNull(ReflectionHelper.findMethod(FindMethod2.class, "test2", String.class));
    assertNotNull(ReflectionHelper.findMethod(FindMethod2.class, "test3"));
    Method m1 = ReflectionHelper.findMethod(FindMethod1.class, "test3");
    Method m2 = ReflectionHelper.findMethod(FindMethod2.class, "test3");
    assertThat(m1, is(not(equalTo(m2))));
  }

  public static class FindMethod1 {

    private String test1() {
      return "test1";
    }

    private String test2(String test) {
      return test;
    }

    public void test3() {
    }
  }

  public static class FindMethod2 extends FindMethod1 {

    @Override
    public void test3() {
    }
  }

  @Test
  public void testGetAndSetField() {
    Field1 f = new Field1();
    ReflectionHelper.setField(f, "test1", "test1");
//		ReflectionHelper.setField(f, "test2", "test2"); // TODO
    assertEquals("test1", ReflectionHelper.getField(f, "test1"));
//		assertEquals("test2", ReflectionHelper.getField(f, "test2")); // TODO
  }

  public static class Field1 {

    public String test1;
    private String test2;
  }

  @Test
  public void testToGetter() {
    assertEquals("getTest", ReflectionHelper.toGetter("test"));
    assertEquals("getTestTest", ReflectionHelper.toGetter("testTest"));
    assertEquals("getTest_test", ReflectionHelper.toGetter("test_test"));
  }

  @Test
  public void testToSetter() {
    assertEquals("setTest", ReflectionHelper.toSetter("test"));
    assertEquals("setTestTest", ReflectionHelper.toSetter("testTest"));
    assertEquals("setTest_test", ReflectionHelper.toSetter("test_test"));
  }

  @Test
  public void testInvokeGetterAndSetter() throws NoSuchFieldException, NoSuchMethodException,
      IllegalAccessException, InvocationTargetException {
    TestData td = new TestData();
    Field f = td.getClass().getDeclaredField("text");
    ReflectionHelper.invokeSetter(td, f, "test");
    assertEquals("test", td.getText());
    String text = (String) ReflectionHelper.invokeGetter(td, f);
    assertEquals("test", text);
  }

  private class TestData {

    private String text;

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }
  }

  public static class TestClass {

  }

  public static class ExtendedTestClass extends TestClass {

  }
}
