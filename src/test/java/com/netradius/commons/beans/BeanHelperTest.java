/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.beans;

import lombok.Data;
import lombok.experimental.Accessors;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for BeanHelper.
 *
 * @author Erik R. Jensen
 */
public class BeanHelperTest {

  public class JavaBean1 {

    private String text;
    private int num1;
    private Integer num2;

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }

    public int getNum1() {
      return num1;
    }

    public void setNum1(int num1) {
      this.num1 = num1;
    }

    public Integer getNum2() {
      return num2;
    }

    public void setNum2(Integer num2) {
      this.num2 = num2;
    }
  }

  public class JavaBean2 {

    private String text;
    private Integer num1;
    private int num2;

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }

    public Integer getNum1() {
      return num1;
    }

    public void setNum1(Integer num1) {
      this.num1 = num1;
    }

    public int getNum2() {
      return num2;
    }

    public void setNum2(int num2) {
      this.num2 = num2;
    }
  }

  public class JavaBean3 {

    private String num2;

    public String getNum2() {
      return num2;
    }

    public void setNum2(String num2) {
      this.num2 = num2;
    }
  }

  @Test
  public void testCopyProperties() {
    JavaBean1 b1 = new JavaBean1();
    b1.setText("test");
    b1.setNum1(1);
    b1.setNum2(2);
    JavaBean2 b2 = new JavaBean2();
    BeanHelper.copyProperties(b1, b2);
    assertEquals(b1.getText(), b2.getText());
    assertEquals(new Integer(b1.getNum1()), b2.getNum1());
    b2.setText("untest");
    BeanHelper.copyProperties(b1, b2, "text");
    assertEquals("untest", b2.getText());

    b1 = new JavaBean1();
    b2 = new JavaBean2();
    b2.setText("moo");
    BeanHelper.copyNotNullProperties(b1, b2);
    assertEquals("moo", b2.getText());

    b1 = new JavaBean1();
    b1.setNum2(2);
    JavaBean3 b3 = new JavaBean3();
    Converter c = new Converter() {
      public boolean converts(Class<?> from, Class<?> to) {
        return from.equals(Integer.class) && to.equals(String.class);
      }

      public Object convert(Object o, Class<?> to) {
        return o.toString();
      }
    };
    BeanHelper.copyProperties(b1, b3, new Converter[]{c});
    assertEquals("2", b3.getNum2());
  }

  @Test
  public void testCopyPropertiesByName() {
    JavaBean1 b1 = new JavaBean1();
    b1.setText("test");
    b1.setNum1(1);
    b1.setNum2(2);
    JavaBean2 b2 = new JavaBean2();
    BeanHelper.copyPropertiesByName(b1, b2, "text", "num1", "num2");
    assertEquals(b1.getText(), b2.getText());
    assertEquals(new Integer(b1.getNum1()), b2.getNum1());
    b2.setText("untest");
    BeanHelper.copyPropertiesByName(b1, b2, "num1", "num2");
    assertEquals("untest", b2.getText());

    b1 = new JavaBean1();
    b2 = new JavaBean2();
    b2.setText("moo");
    BeanHelper.copyNotNullPropertiesByName(b1, b2, "text", "num1", "num2");
    assertEquals("moo", b2.getText());

    b1 = new JavaBean1();
    b1.setNum2(2);
    JavaBean3 b3 = new JavaBean3();
    Converter c = new Converter() {
      public boolean converts(Class<?> from, Class<?> to) {
        return from.equals(Integer.class) && to.equals(String.class);
      }

      public Object convert(Object o, Class<?> to) {
        return o.toString();
      }
    };
    BeanHelper.copyPropertiesByName(b1, b3, new Converter[]{c}, "num2");
    assertEquals("2", b3.getNum2());
  }

  @Data
  public class LombokBean1 {

    private String text;
    private Integer num1;
    private int num2;

  }

  @Data
  @Accessors(chain = true)
  public class LombokBean2 {

    private String text;
    private Integer num1;
    private int num2;

  }

  @Test
  public void testLombokCopyProperties() {
    LombokBean1 lb1 = new LombokBean1();
    lb1.setNum1(1);
    lb1.setNum2(2);
    lb1.setText("test");

    LombokBean2 lb2 = new LombokBean2();

    BeanHelper.copyProperties(lb1, lb2);
    assertEquals(new Integer(1), lb2.getNum1());
    assertEquals(2, lb2.getNum2());
    assertEquals("test", lb2.getText());

  }
}
