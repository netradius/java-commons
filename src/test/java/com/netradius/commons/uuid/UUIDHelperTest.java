/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.uuid;

import com.netradius.commons.lang.ArrayHelper;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class UUIDHelperTest {

  private Random random = new Random();

  @Test
  public void testToAndFromBytes() {
    UUID original = UUID.randomUUID();
    byte[] bytes = UUIDHelper.toBytes(original);

    UUID uuid = UUIDHelper.toUUID(bytes);
    assertThat(uuid, equalTo(original));

    uuid = UUIDHelper.toUUID(bytes, 0);
    assertThat(uuid, equalTo(original));

    int offset = random.nextInt(100);
    byte[] b1 = new byte[offset];
    random.nextBytes(b1);
    byte[] b2 = new byte[random.nextInt(100)];
    random.nextBytes(b2);
    byte[] large = ArrayHelper.concat(b1, bytes, b2);
    uuid = UUIDHelper.toUUID(large, offset);
    assertThat(uuid, equalTo(original));
  }

  @Test
  public void testToAndFromString() {
    UUID original = UUID.randomUUID();
    UUID uuid = UUIDHelper.toUUID(original.toString());
    assertThat(uuid, equalTo(original));

    uuid = UUIDHelper.toUUID("Something Something Something");
    assertThat(uuid, nullValue());
  }

  @Test
  public void testTimeUUID() {
    long millis = System.currentTimeMillis();
    UUID uuid = UUIDHelper.toUUID(new Date(millis));
    long millis2 = UUIDHelper.getTimeMillis(uuid);
    assertThat(millis2, equalTo(millis));
  }

  @Test
  public void testGetVersion() {
    UUID v1 = UUIDHelper.timeUUID();
    assertThat(UUIDHelper.getVersion(v1), equalTo(1));
    UUID v4 = UUIDHelper.randomUUID();
    assertThat(UUIDHelper.getVersion(v4), equalTo(4));
  }

}
