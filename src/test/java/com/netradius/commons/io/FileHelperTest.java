/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.io;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit tests for the FileHelper class.
 *
 * @author Erik R. Jensen
 */
public class FileHelperTest {

  @Test
  public void testDelete() throws IOException {
    final File dir = new File("target" + File.separator + "unittest-tmp");
    dir.deleteOnExit();
    final File file = new File(dir, "unittest.txt");
    file.deleteOnExit();
    dir.mkdir();
    file.createNewFile();
    FileHelper.delete(dir);
    assertFalse(dir.exists());
  }

  @Test
  public void testCopy() throws IOException {
    FileInputStream in = null;
    FileOutputStream out = null;
    try {
      String src = "target" + File.separator + "src.dat";
      String dest = "target" + File.separator + "dest.dat";
      out = new FileOutputStream(src);
      Random rand = new Random();
      int size = rand.nextInt(10240);
      byte[] buf = new byte[size];
      rand.nextBytes(buf);
      out.write(buf);
      FileHelper.copy(new File(src), new File(dest));
      byte[] dbuf = new byte[size];
      in = new FileInputStream(dest);
      assertTrue(size == in.read(dbuf));
      for (int i = 0; i < size; i++) {
        assertTrue(buf[i] == dbuf[i]);
      }
    } finally {
      IOHelper.close(in, out);
    }
  }
}
