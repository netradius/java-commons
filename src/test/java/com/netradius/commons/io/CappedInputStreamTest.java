/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for CappedInputStream.
 *
 * @author Erik R. Jensen
 */
public class CappedInputStreamTest {

  private final Random rand = new Random(System.currentTimeMillis());
  private byte[] b;
  private int cap;

  @BeforeEach
  public void before() {
    System.out.println("MOOOOO");
    b = new byte[1 + rand.nextInt(10485760)];
    rand.nextBytes(b);
    cap = 1 + rand.nextInt(b.length - 2);
  }

  @Test
  public void test1() {
    assertThrows(CappedInputException.class, () -> {
      CappedInputStream cin = new CappedInputStream(new ByteArrayInputStream(b), cap);
      try {
        while (cin.read() != -1) {/* do nothing */}
      } catch (IOException x) {
        assertEquals(cap, cin.getCount());
        throw x;
      }
      fail();
    });
  }

  @Test
  public void test2() {
    assertThrows(CappedInputException.class, () -> {
      CappedInputStream cin = new CappedInputStream(new ByteArrayInputStream(b), cap);
      byte[] buf = new byte[1 + rand.nextInt(8192)];
      try {
        while (cin.read(buf) != -1) {/* do nothing */}
      } catch (IOException x) {
        assertEquals(cap, cin.getCount());
        throw x;
      }
      fail();
    });
  }

  @Test
  public void test3() {
    assertThrows(CappedInputException.class, () -> {
      CappedInputStream cin = new CappedInputStream(new ByteArrayInputStream(b), cap);
      byte[] buf = new byte[1 + rand.nextInt(8192)];
      try {
        while (cin.read(buf, 0, buf.length) != -1) {/* do nothing */}
      } catch (IOException x) {
        assertEquals(cap, cin.getCount());
        throw x;
      }
      fail();
    });
  }
}
