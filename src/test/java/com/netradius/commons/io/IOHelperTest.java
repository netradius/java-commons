/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.commons.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/**
 * Unit tests for the IOHelper class.
 *
 * @author Erik R. Jensen
 */
public class IOHelperTest {

  private final Random rand = new Random(System.currentTimeMillis());

  private final byte[] b = new byte[2048];

  @BeforeEach
  public void before() {
    rand.nextBytes(b);
  }

  @Test
  public void testFill() throws IOException {
    InputStream in = new RandomReadInputStream();
    byte[] buf = new byte[2048];
    IOHelper.fill(in, buf);
    assertArrayEquals(b, buf);
  }

  @Test
  public void testCopy() throws IOException {
    byte[] buf1 = new byte[1337];
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    rand.nextBytes(buf1);

    IOHelper.copy(new ByteArrayInputStream(buf1), out);
    assertArrayEquals(buf1, out.toByteArray());

    out = new ByteArrayOutputStream();
    IOHelper.copy(new ByteArrayInputStream(buf1), out, 1024);
    assertArrayEquals(buf1, out.toByteArray());

    out = new ByteArrayOutputStream();
    IOHelper.copy(new ByteArrayInputStream(buf1), out, 1024l, 1021);
    assertArrayEquals(Arrays.copyOf(buf1, 1024), out.toByteArray());

    out = new ByteArrayOutputStream();
    IOHelper.copy(new ByteArrayInputStream(buf1), out, 1024l, 1);
    assertArrayEquals(Arrays.copyOf(buf1, 1024), out.toByteArray());
  }

  private class RandomReadInputStream extends InputStream {

    int idx = 0;

    @Override
    public int read() {
      return idx == b.length ? -1 : b[idx++] & 0xff;
    }

    @Override
    public int read(byte[] buf) {
      for (int i = 0, r = rand.nextInt(buf.length - 1); i < buf.length; i++) {
        if (i == r) {
          return i;
        }
        int v = read();
        if (v == -1) {
          return i == 0 ? -1 : i;
        } else {
          buf[i] = (byte) v;
        }
      }
      return buf.length;
    }
  }
}
